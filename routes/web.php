<?php

use Illuminate\Http\Response;

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/all', function(){
   return view('all');
});

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/dashboard', 'Dashboard@index')->name('dashboard');
Route::match(
    ['get', 'post'],
    '/',
    'Dashboard@index'
)->name('dashboard');

Route::get('/calltrack', 'CallTracking@index')->name('calltrack');
//Route::get('/contents/calltrack/count-calls', 'CallTracking@getCountCalls')->name('contents.calltrack.count-calls');

Route::get('/yandex', 'Yandex@index')->name('yandex');
//Route::get('/contents/yandex/campaigns-list', 'Yandex@getCampaignsList')->name('contents.yandex.campaigns-list');
//Route::get('/contents/yandex/campaigns-info', 'Yandex@getReport')->name('contents.yandex.campaigns-info');

Route::get('/facebook', 'Facebook@index')->name('facebook');
//Route::get('/contents/facebook/campaigns-list', 'Facebook@getCampaignsList')->name('contents.facebook.campaigns-list');
//Route::get('/contents/facebook/{campaignID}/campaigns-info', 'Facebook@getCampaignInfo')->name('contents.facebook.campaigns-info');
/*Route::get('/contents/facebook/{campaignID}/campaigns-info', function ($campaignID) {
    return $campaignID;
});*/

Route::get('/google', 'Google@index')->name('google');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function () {
    Route::resource('/users', 'UsersController', ['except' => ['show']]);
});
Route::get('/properties', 'Properties@index')->name('admin.properties.index');

Route::get('/compare', 'Dashboard@compareIndex')->name('compare');

Route::match(
    ['get', 'post'],
    'compare/list',
    'Dashboard@getCompare'
);

Route::match(
    ['get', 'post'],
    'google/get-campaigns',
    'AdWordsApiController@getCampaignsAction'
);
Route::match(
    ['get', 'post'],
    'google/download-report',
    'AdWordsApiController@downloadReportAction'
);

Route::match(
    ['get', 'post'],
    'yandex/get-campaigns',
    'Yandex@getCampaignsList'
);

Route::match(
    ['get', 'post'],
    'yandex/report-results',
    'Yandex@getReport'
);

Route::match(
    ['get', 'post'],
    'calltrack/count-calls',
    'CallTracking@getCountCalls'
);

Route::match(
    ['get', 'post'],
    'facebook/get-campaigns',
    'Facebook@getCampaignsList'
);
Route::match(
    ['get', 'post'],
    'facebook/campaign-info',
    'Facebook@getCampaignInfo'
);

Route::match(
  ['get', 'post'],
  'properties/update',
  'Properties@updatePropertiesList'
);

Route::match(
    ['get', 'post'],
    'properties/desc',
    'Properties@getPropertyDescription'
);

Route::match(
    ['get', 'post'],
    'yandex/info',
    'Yandex@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'facebook/info',
    'Facebook@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'calltrack/info',
    'CallTracking@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'google/info',
    'AdWordsApiController@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'leads',
    'Leads@index'
)->name('leads');

Route::match(
    ['get', 'post'],
    'leads/load',
    'Leads@loadLeads'
);

Route::match(
    ['get', 'post'],
    'leads/update',
    'Leads@updateLead'
);

Route::match(
    ['get', 'post'],
    'leads/download',
    'Leads@downloadLeads'
);
