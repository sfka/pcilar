$(document).ready(function () {
    sendAjaxForm();
    putCampaignInput();
    allCheckbox();
    getDashboardInfo();
    initDropdown();
    initDatepicker();
    initSelect();
    getPropDescription();
    initTooltip();
    onSelectReportType();
    leadUpdateValid()
    $("#reportType").trigger("change");
});

function sendAjaxForm() {
    $('body').on('submit', '.ajax__form', function (event) {
        event.preventDefault();
        var elem = $(this);
        var url = elem.attr('action');
        var leadIDs = elem.find('input[name="leadids"]').val();
        if (typeof leadIDs !== 'undefined') {
            elem.find('input[name="leadids"]').val(eval(leadIDs));
        }
        var data = elem.serialize();

        if (elem.hasClass('file_form')) {
            data = new FormData(elem[0]);
            $.ajaxSetup({
                cache: false,
                contentType: false,
                processData: false,
                dataType: 'script'
            });
        }

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $('body').addClass('preloader');
            },
            success: function (res) {
                if (elem.attr('id') == 'campaign_input') {
                    $('.content_form_info').html(res);
                    content = $('.content_form_info');
                } else if ($(res).find('.content_form').length) {
                    var html = $(res).find('.content_form');
                    $('.content_form').html(html);
                    content = $('.content_form');
                } else {
                    $('.content_form').html(res);
                    content = $('.content_form');
                }
                $('html, body').animate({
                    scrollTop: content.offset().top - 70
                }, {
                    duration: 370,
                    easing: "linear"
                });
                $('body').removeClass('preloader');
                initTooltip();
            }
        });
    });
}

function putCampaignInput() {
    $('body').on('click', '.js__campaign_input', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        $('#campaignID').val(id);
        $('html, body').animate({
            scrollTop: $('#campaignID').closest('form').offset().top - 70
        }, {
            duration: 370,
            easing: "linear"
        });
    });
}

function allCheckbox() {
    $('body').on('change', '.all_checkbox', function () {
        if (this.checked) {
            $(this).closest('.checkbox-list').find('input[type="checkbox"]').prop('checked', true);
        } else {
            $(this).closest('.checkbox-list').find('input[type="checkbox"]').prop('checked', false);
        }
    });
}

function getDashboardInfo() {
    $('[name="platform[]"]').each(function (index, elem) {
        var url = $(this).val().toLowerCase() + '/info';
        var form = $(this).closest('form');
        var data = form.serialize();
        $.ajax({
            //type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
            },
            success: function (res) {
                $('#ajax_content').append(res);
            }
        });
    });
}

function changePlatformDashboard(moreUrl = '') {
    $(document).off('change', '#platform').on('change', '#platform', function (event) {
        var elem = $(this);
        var form = elem.closest('form');
        var url = '/' + elem.val().toLowerCase() + moreUrl;
        form.attr('action', url);
        var data = form.serialize();
        $.ajax({
            //type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
            },
            success: function (res) {
                $('#ajax_content').html(res);
            }
        });
    });
}

function addNewPropRow(elem) {
    var table = elem.closest('form').find('table');
    var clone = table.find('tr').last().clone();
    var lastID = parseInt(clone.attr('data-id'));
    if (isNaN(lastID) === true) {
        var newId = 1;
    } else {
        var newId = lastID + 1;
    }
    console.log(newId);
    var newProp = '<tr data-id="' + newId + '"><td>' + newId + '</td> <td><div class="input-field"><input type="text" name="code[' + newId + ']" value=""> <label class="active">Код</label></div></td> <td><div class="input-field"><input type="text" name="name[' + newId + ']" value=""> <label class="active">Имя</label></div></td> <td><div class="input-field"><input type="text" name="description[' + newId + ']" value=""> <label>Описание</label></div></td></tr>';
    table.append(newProp);
}

function initDropdown() {
    let elems = document.querySelectorAll('.dropdown-trigger');
    let instances = M.Dropdown.init(elems, {});
}

function initDatepicker() {
    let elems = document.querySelectorAll('.datepicker');
    let instances = M.Datepicker.init(elems, {
        format: 'yyyy-mm-dd',
        maxDate: new Date(),
        yearRange: [new Date().getFullYear() - 5, new Date().getFullYear()],
        i18n: {
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekdaysAbbrev: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            cancel: 'Отмена',
            clear: 'Очистить',
            done: 'Ок'
        }
    });
}

function initSelect() {
    let elems = document.querySelectorAll('select');
    let instances = M.FormSelect.init(elems, {});
}

function initTooltip() {
    let elems = document.querySelectorAll('.tooltipped');
    let instances = M.Tooltip.init(elems, {
        enterDelay: 200
    });
}

function getPropDescription() {
    if ($("label").is("[data-field]")) {
        prop = [];
        $('[data-field]').each(function () {
            prop.push($(this).attr('data-field'));
        });
        var url = '/properties/desc';
        var token = $('meta[name="csrf-token"]').attr('content');
        var data = {
            _token: token,
            property: prop,
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json',
            beforeSend: function () {
            },
            success: function (res) {
                $.each(res, function (i, item) {
                    if (typeof item.code !== 'undefined') {
                        if (item.name !== null) $('[data-field="' + i + '"]').attr('data-tooltip', '<b>' + item.name + '</b>');
                        if (item.description !== null) $('[data-field="' + i + '"]').attr('data-tooltip', item.description);
                        if (item.name !== null && item.description !== null) $('[data-field="' + i + '"]').attr('data-tooltip', '<b>' + item.name + '</b><br>' + item.description);
                    } else {
                        $('[data-field="' + i + '"]').attr('data-tooltip', item.error);
                    }
                });
            }
        });
    }
}

function onSelectReportType() {
    $(document).off('change', '#reportType').on('change', '#reportType', function (event) {
        var type = $(this).val();
        var form = $(this).closest('form');
        var label = form.find('label.js__label');
        var labelActive = form.find('label.js__label.' + type);
        label.addClass('hidden');
        labelActive.removeClass('hidden');
        label.find('input').prop('checked', false);
        labelActive.find('input').prop('checked', true);
    });
}

function generateText(elID) {
    let randText = Math.random().toString(36).substring(7);
    if (typeof elID !== 'undefined') {
        document.getElementById(elID).value = randText;
    }
}

function leadUpdateValid() {
    $('body').on('change', '.ajax__lead_update', function (e) {
        if ($(this).attr('name') == 'valid') {
            if($(this).prop('checked') === false) {
                $(this).prop('checked', true);
            }
            var parent = $(this).closest('td');
            parent.find('input[type="checkbox"]').not(this).prop('checked', false);
            var checked = $(this).val();
        }
        if ($(this).attr('name') == 'comment') {
            var comment = $(this).val();
        }
        var url = '/leads/update';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id = $(this).attr('data-id')
        var data = {
            _token: token,
            checked: checked,
            comment: comment,
            id: id,
        };
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            beforeSend: function () {
                $('body').addClass('preloader');
            },
            success: function (res) {
                $('body').removeClass('preloader');
            }
        });
    });
}

var generatePass = document.querySelector('.generate-pass');
if (generatePass) {
    generatePass.addEventListener('click', function () {
        this.closest('.input-field').querySelector('label').classList.add('active');
    });
}

$(document).ready(function(){
    $('.sidenav').sidenav();
});
