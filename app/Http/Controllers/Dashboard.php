<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CallTracking;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Dashboard extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $platforms = [
        'Yandex', 'Facebook', 'Google', 'CallTrack',
    ];

    public function index(Request $request)
    {
        $request->session()->put('timeUpdateSession', strtotime(date('Y-m-d', strtotime('+1 day'))));
        $updateTime = $request->session()->get('timeUpdateSession');
        $fields = $request->all();

        $dateRange = [
            'DateFrom' => (isset($fields['dateFrom1']) ? date('d.m.Y', strtotime($fields['dateFrom1'])) : date('d.m.Y', strtotime('-7 days'))),
            'DateTo' => (isset($fields['dateTo1']) ? date('d.m.Y', strtotime($fields['dateTo1'])) : date('d.m.Y', strtotime('-1 days'))),
        ];
        $dateRangeToSession = [
            'DateFrom' => (isset($fields['dateFrom1']) ? $fields['dateFrom1'] : date('Y-m-d', strtotime('-7 days'))),
            'DateTo' => (isset($fields['dateTo1']) ? $fields['dateTo1'] : date('Y-m-d', strtotime('-1 days'))),
        ];
        if (isset($fields['user_name'])) {
            $userName = $fields['user_name'];
        } else {
            $userName = Auth::user()->id;
        }
        if ($updateTime <= strtotime("now")) {
            $request->session()->forget('yandexClientInfo' . $userName . 'Date_' . $dateRangeToSession['DateFrom'] . '_' . $dateRangeToSession['DateTo']);
            $request->session()->forget('facebookClientInfo' . $userName . 'Date_' . $dateRangeToSession['DateFrom'] . '_' . $dateRangeToSession['DateTo']);
            $request->session()->forget('googleClientInfo' . $userName . 'Date_' . $dateRangeToSession['DateFrom'] . '_' . $dateRangeToSession['DateTo']);
            $request->session()->forget('calltrackClientInfo' . $userName . 'Date_' . $dateRangeToSession['DateFrom'] . '_' . $dateRangeToSession['DateTo']);
        }
        $arResult['dateRange'] = $dateRange;

        if (Auth::user()->hasRole('user')) {
            if (isset($fields['platform'])) {
                $arResult['Info'] = self::getArrayDashboard($fields['platform'], $dateRangeToSession);
            } else {
                foreach ($this->platforms as $platform) {
                    $login = lcfirst($platform) . 'Login';
                    if (isset(Auth::user()->$login)) {
                        $arResult['Info'] = self::getArrayDashboard($platform, $dateRangeToSession);
                        break;
                    }
                }
            }
        } elseif (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager')) {
            $arResult['USERS'] = Role::where('name', 'user')->first()->users()->get();
            if (isset($fields['user_name'])) {
                $arResult['Info'] = self::getArrayDashboard($fields['platform'], $dateRangeToSession, NULL, $fields['user_name']);
            }
        }
        return view('dashboard')->with([
            'arResult' => $arResult,
        ]);
    }

    public function compareIndex()
    {
        return view('compare')->with([
        ]);
    }

    public function getCompare(Request $request)
    {
        $fields = $request->all();
        if (isset($fields['dateFrom1']) && isset($fields['dateTo1']) && isset($fields['dateFrom2']) && isset($fields['dateTo2'])) {
            $defaultDate1 = [
                'DateFrom' => $fields['dateFrom1'],
                'DateTo' => $fields['dateTo1'],
            ];
            $defaultDate2 = [
                'DateFrom' => $fields['dateFrom2'],
                'DateTo' => $fields['dateTo2'],
            ];
        } else {
            $defaultDate1 = [
                'DateFrom' => date('Y-m-d', strtotime('-7 days')),
                'DateTo' => date('Y-m-d', strtotime('-1 days')),
            ];
            $defaultDate2 = [
                'DateFrom' => date('Y-m-d', strtotime('-14 days')),
                'DateTo' => date('Y-m-d', strtotime('-8 days')),
            ];
        }

        if (isset($fields['platform'])) {
            $arResult = self::getArrayDashboard($fields['platform'], $defaultDate1, $defaultDate2);
        }
        return view('contents.compare.compare-list')->with([
            'arResult' => $arResult,
        ]);
    }

    /**
     * @param $platform
     * @param $date1
     * @param null $date2
     * @param null $userID
     * @return mixed
     */
    public function getArrayDashboard($platform, $date1, $date2 = NULL, $userID = NULL)
    {
        switch ($platform) {
            case 'Yandex':
                $yandex = new Yandex();
                $yandexClientInfo1 = $yandex->getClientInfo($date1['DateFrom'], $date1['DateTo'], $userID);
                $arResult['YANDEX_INFO']['1'] = $yandexClientInfo1;
                if ($date2 != NULL) {
                    $yandexClientInfo2 = $yandex->getClientInfo($date2['DateFrom'], $date2['DateTo'], $userID);
                    $arResult['YANDEX_INFO']['2'] = $yandexClientInfo2;
                }
                break;
            case 'Google':
                $google = new AdWordsApiController();
                $googleClientInfo1 = $google->getClientInfo($date1['DateFrom'], $date1['DateTo'], $userID);
                $arResult['GOOGLE_INFO']['1'] = $googleClientInfo1;
                if ($date2 != NULL) {
                    $googleClientInfo2 = $google->getClientInfo($date2['DateFrom'], $date2['DateTo'], $userID);
                    $arResult['GOOGLE_INFO']['2'] = $googleClientInfo2;
                }
                break;
            case 'Facebook':
                $facebook = new Facebook();
                $facebookClientInfo1 = $facebook->getClientInfo($date1['DateFrom'], $date1['DateTo'], $userID);
                $arResult['FACEBOOK_INFO']['1'] = $facebookClientInfo1;
                if ($date2 != NULL) {
                    $facebookClientInfo2 = $facebook->getClientInfo($date2['DateFrom'], $date2['DateTo'], $userID);
                    $arResult['FACEBOOK_INFO']['2'] = $facebookClientInfo2;
                }
                break;
            case 'CallTrack':
                $calltrackClientInfo1 = CallTracking::getClientInfo($date1['DateFrom'], $date1['DateTo'], $userID);
                $arResult['CALLTRACK_INFO']['1'] = $calltrackClientInfo1;
                if ($date2 != NULL) {
                    $calltrackClientInfo2 = CallTracking::getClientInfo($date2['DateFrom'], $date2['DateTo'], $userID);
                    $arResult['CALLTRACK_INFO']['2'] = $calltrackClientInfo2;
                }
                break;
        }
        $arResult['Date']['1'] = $date1;
        if ($date2 != NULL) {
            $arResult['Date']['2'] = $date2;
        }

        $arResult['Date']['1']['DateFrom'] = date('d.m.Y', strtotime($date1['DateFrom']));
        $arResult['Date']['1']['DateTo'] = date('d.m.Y', strtotime($date1['DateTo']));
        if ($date2 != NULL) {
            $arResult['Date']['2']['DateFrom'] = date('d.m.Y', strtotime($date2['DateFrom']));
            $arResult['Date']['2']['DateTo'] = date('d.m.Y', strtotime($date2['DateTo']));
        }
        return $arResult;
    }

    public static function getCompareNumber($num1, $num2)
    {
        $text = '';
        if($num1 < $num2) {
            $text .= '<span><span class="minus-triangle triangle"></span>';
            $perc = round(100 - round($num1 / $num2 * 100, 1), 1);
            $text .= $perc . '%</span>';
        }
        else {
            $text .= '<span><span class="plus-triangle triangle"></span>';
            $perc =  round(round($num1 / $num2 * 100, 1) - 100, 1);
            $text .= $perc . '%</span>';
        }
        return $text;
    }
}
