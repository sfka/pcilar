<?php

/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Http\Controllers;

use App\User;
use Google\AdsApi\AdManager\v201908\Location;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Query\v201809\ReportQueryBuilder;
use Google\AdsApi\AdWords\Query\v201809\ServiceQueryBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Google\AdsApi\AdWords\v201809\billing\BillingAccount;
use Google\AdsApi\AdWords\v201809\billing\BudgetOrder;
use Google\AdsApi\AdWords\v201809\billing\BudgetOrderService;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\o\TrafficEstimatorService;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\Auth\FetchAuthTokenInterface;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionService;

class AdWordsApiController extends Controller
{

    private static $REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS = [
        'CAMPAIGN_PERFORMANCE_REPORT' => [
            'CampaignName',
            'CampaignStatus',
            'AccountDescriptiveName',
            'Amount',
            //'AccountCurrencyCode', 'AccountTimeZone', 'AdvertisingChannelSubType', 'AdvertisingChannelType', 'Amount', 'BaseCampaignId', 'BiddingStrategyId', 'BiddingStrategyName', 'BiddingStrategyType', 'BudgetId', 'CampaignDesktopBidModifier', 'CampaignGroupId', 'CampaignMobileBidModifier', 'CampaignTabletBidModifier', 'CampaignTrialType', 'ConversionAdjustment', 'CustomerDescriptiveName', 'EndDate', 'EnhancedCpcEnabled', 'ExternalCustomerId', 'FinalUrlSuffix', 'HasRecommendedBudget', 'IsBudgetExplicitlyShared', 'LabelIds', 'Labels', 'MaximizeConversionValueTargetRoas', 'Period', 'RecommendedBudgetAmount', 'ServingStatus', 'StartDate', 'TotalAmount', 'TrackingUrlTemplate', 'UrlCustomParameters',
        ],
        'ADGROUP_PERFORMANCE_REPORT' => [
            'AdGroupName',
            'AdGroupStatus',
            'CampaignId',
            //'AccountCurrencyCode', 'AccountDescriptiveName', 'AccountTimeZone', 'AdGroupDesktopBidModifier', 'AdGroupMobileBidModifier', 'AdGroupTabletBidModifier', 'AdGroupType', 'AdRotationMode', 'BaseAdGroupId', 'BaseCampaignId', 'BiddingStrategyId', 'BiddingStrategyName', 'BiddingStrategySource', 'BiddingStrategyType', 'CampaignName', 'CampaignStatus', 'ContentBidCriterionTypeGroup', 'ConversionAdjustment', 'CpcBid', 'CpmBid', 'CpvBid', 'CustomerDescriptiveName', 'EffectiveTargetRoas', 'EffectiveTargetRoasSource', 'EnhancedCpcEnabled', 'ExternalCustomerId', 'FinalUrlSuffix', 'LabelIds', 'Labels', 'TargetCpa', 'TargetCpaBidSource', 'TrackingUrlTemplate', 'UrlCustomParameters',
        ],
        'AD_PERFORMANCE_REPORT' => [
            'AdGroupName', 'AdType',
            //'AccentColor', 'AccountCurrencyCode', 'AccountDescriptiveName', 'AccountTimeZone', 'AdGroupStatus', 'AdStrengthInfo', 'AllowFlexibleColor', 'Automated', 'BaseAdGroupId', 'BaseCampaignId', 'BusinessName', 'CallOnlyPhoneNumber', 'CallToActionText', 'CampaignId', 'CampaignName', 'CampaignStatus', 'CombinedApprovalStatus', 'ConversionAdjustment', 'CreativeDestinationUrl', 'CreativeFinalAppUrls', 'CreativeFinalMobileUrls', 'CreativeFinalUrls', 'CreativeFinalUrlSuffix', 'CreativeTrackingUrlTemplate', 'CreativeUrlCustomParameters', 'CustomerDescriptiveName', 'Description', 'Description1', 'Description2', 'DevicePreference', 'DisplayUrl', 'EnhancedDisplayCreativeLandscapeLogoImageMediaId', 'EnhancedDisplayCreativeLogoImageMediaId', 'EnhancedDisplayCreativeMarketingImageMediaId', 'EnhancedDisplayCreativeMarketingImageSquareMediaId', 'ExpandedDynamicSearchCreativeDescription2', 'ExpandedTextAdDescription2', 'ExpandedTextAdHeadlinePart3', 'ExternalCustomerId', 'FormatSetting', 'GmailCreativeHeaderImageMediaId', 'GmailCreativeLogoImageMediaId', 'GmailCreativeMarketingImageMediaId', 'GmailTeaserBusinessName', 'GmailTeaserDescription', 'GmailTeaserHeadline', 'Headline', 'HeadlinePart1', 'HeadlinePart2', 'ImageAdUrl', 'ImageCreativeImageHeight', 'ImageCreativeImageWidth', 'ImageCreativeMimeType', 'ImageCreativeName', 'IsNegative', 'LabelIds', 'Labels', 'LongHeadline', 'MainColor', 'MarketingImageCallToActionText', 'MarketingImageCallToActionTextColor', 'MarketingImageDescription', 'MarketingImageHeadline', 'MultiAssetResponsiveDisplayAdAccentColor', 'MultiAssetResponsiveDisplayAdAllowFlexibleColor', 'MultiAssetResponsiveDisplayAdBusinessName', 'MultiAssetResponsiveDisplayAdCallToActionText', 'MultiAssetResponsiveDisplayAdDescriptions', 'MultiAssetResponsiveDisplayAdDynamicSettingsPricePrefix', 'MultiAssetResponsiveDisplayAdDynamicSettingsPromoText', 'MultiAssetResponsiveDisplayAdFormatSetting', 'MultiAssetResponsiveDisplayAdHeadlines', 'MultiAssetResponsiveDisplayAdLandscapeLogoImages', 'MultiAssetResponsiveDisplayAdLogoImages', 'MultiAssetResponsiveDisplayAdLongHeadline', 'MultiAssetResponsiveDisplayAdMainColor', 'MultiAssetResponsiveDisplayAdMarketingImages', 'MultiAssetResponsiveDisplayAdSquareMarketingImages', 'MultiAssetResponsiveDisplayAdYouTubeVideos', 'Path1', 'Path2', 'PolicySummary', 'PricePrefix', 'PromoText', 'ResponsiveSearchAdDescriptions', 'ResponsiveSearchAdHeadlines', 'ResponsiveSearchAdPath1', 'ResponsiveSearchAdPath2', 'ShortHeadline', 'Status', 'SystemManagedEntitySource', 'UniversalAppAdDescriptions', 'UniversalAppAdHeadlines', 'UniversalAppAdHtml5MediaBundles', 'UniversalAppAdImages', 'UniversalAppAdMandatoryAdText', 'UniversalAppAdYouTubeVideos',
        ],
        'ACCOUNT_PERFORMANCE_REPORT' => [
            'AccountDescriptiveName',
            'ExternalCustomerId',
            'Date',
            //'AccountCurrencyCode', 'AccountTimeZone', 'CanManageClients', 'ConversionAdjustment', 'CustomerDescriptiveName', 'IsAutoTaggingEnabled', 'IsTestAccount',
        ],
        'BUDGET_PERFORMANCE_REPORT' => [
            'BudgetName', 'Amount', 'TotalAmount', 'IsBudgetExplicitlyShared',
        ],
    ];

    /**
     * Controls a POST and GET request that is submitted from the "Get All
     * Campaigns" form.
     *
     * @param Request $request
     * @param FetchAuthTokenInterface $oAuth2Credential
     * @param AdWordsServices $adWordsServices
     * @param AdWordsSessionBuilder $adWordsSessionBuilder
     * @return View
     */
    public function getCampaignsAction(
        Request $request,
        FetchAuthTokenInterface $oAuth2Credential,
        AdWordsServices $adWordsServices,
        AdWordsSessionBuilder $adWordsSessionBuilder
    )
    {
        if ($request->method() === 'POST') {
            // Always select at least the "Id" field.
            $fieldNames = [];
            if ($request->input('fields')) {
                foreach ($request->input('fields') as $field) {
                    $fieldNames[] = $field;
                }
            }
            $selectedFields = array_values(
                ['id' => 'Id'] + $request->except(
                    ['_token', 'clientCustomerId', 'entriesPerPage', 'fields']
                )
            );
            $selectedFields = array_merge($selectedFields, $fieldNames);
            //$clientCustomerId = $request->input('clientCustomerId');
            $clientCustomerId = Auth::user()->googleLogin;
            $entriesPerPage = $request->input('entriesPerPage');

            // Construct an API session configured from a properties file and
            // the OAuth2 credentials above.
            $session =
                $adWordsSessionBuilder->fromFile(config('app.adsapi_php_path'))
                    ->withOAuth2Credential($oAuth2Credential)
                    ->withClientCustomerId($clientCustomerId)
                    ->build();

            $request->session()->put('selectedFields', $selectedFields);
            $request->session()->put('entriesPerPage', $entriesPerPage);
            $request->session()->put('session', $session);
        } else {
            $selectedFields = $request->session()->get('selectedFields');
            $entriesPerPage = $request->session()->get('entriesPerPage');
            $session = $request->session()->get('session');
        }

        $pageNo = $request->input('page') ?: 1;
        $collection = self::fetchCampaigns(
            $request,
            $adWordsServices->get($session, CampaignService::class),
            $selectedFields,
            $entriesPerPage,
            $pageNo
        );

        // Create a length aware paginator to supply campaigns for the view,
        // based on the specified number of entries per page.
        $campaigns = new LengthAwarePaginator(
            $collection,
            $request->session()->get('totalNumEntries'),
            $entriesPerPage,
            $pageNo,
            ['path' => url('google/get-campaigns')]
        );

        return view('contents.google.campaigns', compact('campaigns', 'selectedFields'));
    }

    /**
     * Fetch campaigns using the provided campaign service, selected fields, the
     * number of entries per page and the specified page number.
     *
     * @param Request $request
     * @param CampaignService $campaignService
     * @param string[] $selectedFields
     * @param int $entriesPerPage
     * @param int $pageNo
     * @return Collection
     */
    private function fetchCampaigns(
        Request $request,
        CampaignService $campaignService,
        array $selectedFields,
        $entriesPerPage,
        $pageNo
    )
    {
        $query = (new ServiceQueryBuilder())
            ->select($selectedFields)
            ->orderByAsc('Name')
            ->limit(
                ($pageNo - 1) * $entriesPerPage,
                intval($entriesPerPage)
            )->build();

        $totalNumEntries = 0;
        $results = [];

        $page = $campaignService->query("$query");
        if (!empty($page->getEntries())) {
            $totalNumEntries = $page->getTotalNumEntries();
            $results = $page->getEntries();
        }

        $request->session()->put('totalNumEntries', $totalNumEntries);

        return collect($results);
    }

    /**
     * Controls a POST and GET request that is submitted from the "Download
     * Report" form.
     *
     * @param Request $request
     * @param FetchAuthTokenInterface $oAuth2Credential
     * @param AdWordsServices $adWordsServices
     * @param AdWordsSessionBuilder $adWordsSessionBuilder
     * @return View
     */
    public function downloadReportAction(
        Request $request,
        FetchAuthTokenInterface $oAuth2Credential,
        AdWordsServices $adWordsServices,
        AdWordsSessionBuilder $adWordsSessionBuilder
    )
    {
        if ($request->method() === 'POST') {
            //$clientCustomerId = $request->input('clientCustomerId');
            $clientCustomerId = Auth::user()->googleLogin;
            $reportType = $request->input('reportType');

            $entriesPerPage = $request->input('entriesPerPage');

            $selectedFields = array_values(
                $request->except(
                    [
                        '_token',
                        'clientCustomerId',
                        'reportType',
                        'entriesPerPage',
                        'dateFrom',
                        'dateTo',
                    ]
                )
            );
            if ($request->input('dateFrom')) {
                $date['DateFrom'] = date('Ymd', strtotime($request->input('dateFrom')));
            } else {
                $date['DateFrom'] = date('Ymd', strtotime('-7 days'));
            }
            if ($request->input('dateTo')) {
                $date['DateTo'] = date('Ymd', strtotime($request->input('dateTo')));
            } else {
                $date['DateTo'] = date('Ymd', strtotime('-1 days'));
            }
            $reportRange = $date;
            $selectedFields = array_merge(
                self::$REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS[$reportType],
                $selectedFields
            );

            $request->session()->put('clientCustomerId', $clientCustomerId);
            $request->session()->put('reportType', $reportType);
            $request->session()->put('reportRange', $reportRange);
            $request->session()->put('selectedFields', $selectedFields);
            $request->session()->put('entriesPerPage', $entriesPerPage);

            // Construct an API session configured from a properties file and
            // the OAuth2 credentials above.
            $session =
                $adWordsSessionBuilder->fromFile(config('app.adsapi_php_path'))
                    ->withOAuth2Credential($oAuth2Credential)
                    ->withClientCustomerId($clientCustomerId)
                    ->build();

            // There is no paging mechanism for reporting, so we fetch all
            // results at once.
            $collection = self::downloadReport(
                $reportType,
                $reportRange,
                new ReportDownloader($session),
                $selectedFields
            );
            $request->session()->put('collection', $collection);
        } else {
            $selectedFields = $request->session()->get('selectedFields');
            $entriesPerPage = $request->session()->get('entriesPerPage');
            $collection = $request->session()->get('collection');
        }

        $pageNo = $request->input('page') ?: 1;

        // Create a length aware paginator to supply report results for the
        // view, based on the specified number of entries per page.
        $reportResults = new LengthAwarePaginator(
            $collection->forPage($pageNo, $entriesPerPage),
            $collection->count(),
            $entriesPerPage,
            $pageNo,
            ['path' => url('google/download-report')]
        );

        return view(
            'contents.google.report-results',
            compact('reportResults', 'selectedFields')
        );
    }

    /**
     * Download a report of the specified report type and date range, selected
     * fields, and the number of entries per page.
     *
     * @param string $reportType
     * @param string $reportRange
     * @param ReportDownloader $reportDownloader
     * @param string[] $selectedFields
     * @return Collection
     */
    private function downloadReport(
        $reportType,
        $reportRange,
        ReportDownloader $reportDownloader,
        array $selectedFields
    )
    {
        $query = (new ReportQueryBuilder())
            ->select($selectedFields)
            ->from($reportType)
            ->during($reportRange['DateFrom'], $reportRange['DateTo'])->build();

        // For brevity, this sample app always excludes zero-impression rows.
        $reportSettingsOverride = (new ReportSettingsBuilder())
            ->includeZeroImpressions(false)
            ->build();
        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            "$query",
            DownloadFormat::XML,
            $reportSettingsOverride
        );

        $json = json_encode(
            simplexml_load_string($reportDownloadResult->getAsString())
        );
        $resultTable = json_decode($json, true)['table'];

        if (array_key_exists('row', $resultTable)) {
            // When there is only one row, PHP decodes it by automatically
            // removing the containing array. We need to add it back, so the
            // "view" can render this data properly.
            $row = $resultTable['row'];
            $row = count($row) > 1 ? $row : [$row];
            return collect($row);
        }

        // No results returned for this query.
        return collect([]);
    }

    public function getClientInfo($dateFrom = NULL, $dateTo = NULL, $userID)
    {
        if ($userID == NULL) {
            $googleLogin = Auth::user()->googleLogin;
            $userName = Auth::user()->id;
        } else {
            $user = DB::table('users')->where('id', $userID)->first();
            $googleLogin = $user->googleLogin;
            $userName = $user->id;
        }
        if ($dateFrom == NULL) {
            $date['DateFrom'] = date('Ymd', strtotime('-7 days'));
        } else {
            $date['DateFrom'] = date('Ymd', strtotime($dateFrom));
        }
        if ($dateTo == NULL) {
            $date['DateTo'] = date('Ymd', strtotime('-1 days'));
        } else {
            $date['DateTo'] = date('Ymd', strtotime($dateTo));
        }
        $sessionDate['DateFrom'] = date('Y-m-d', strtotime($date['DateFrom']));
        $sessionDate['DateTo'] = date('Y-m-d', strtotime($date['DateTo']));
        if (isset($googleLogin) && $googleLogin != '') {
            if (Session::get('googleClientInfo' . $userName . 'Date_' . $sessionDate['DateFrom'] . '_' . $sessionDate['DateTo'])) {
                $googleClientInfo = Session::get('googleClientInfo' . $userName . 'Date_' . $sessionDate['DateFrom'] . '_' . $sessionDate['DateTo']);
            } else {
                $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(config('app.adsapi_php_path'))->build();
                $session = (new AdWordsSessionBuilder())
                    ->fromFile(config('app.adsapi_php_path'))
                    ->withOAuth2Credential($oAuth2Credential)
                    ->withClientCustomerId($googleLogin)
                    ->build();
                $reportType = 'ACCOUNT_PERFORMANCE_REPORT';
                $reportRange = $date;
                $selectedFields = [
                    'AccountDescriptiveName', 'Impressions', 'Clicks', 'Cost', 'AverageCpc', 'Conversions', 'ConversionValue', 'CostPerAllConversion', 'CostPerConversion'
                ];
                $collection = self::downloadReport(
                    $reportType,
                    $reportRange,
                    new ReportDownloader($session),
                    $selectedFields
                );
                $reportRange['DateFrom'] = date('Ymd', strtotime('2015-01-01'));
                $reportRange['DateTo'] = date('Ymd', time());
                $collectionAllTime = self::downloadReport(
                    $reportType,
                    $reportRange,
                    new ReportDownloader($session),
                    $selectedFields
                );
                $googleClientInfo = $collection[0]['@attributes'];

                $googleClientInfo['Ctr'] = round(($googleClientInfo['clicks'] / $googleClientInfo['impressions']) * 100, 1);
                $budget = self::getSpendingLimit($googleLogin);

                $googleClientInfo['balance'] = $budget - round($collectionAllTime[0]['@attributes']['cost'] / 1000000, 2);
                $dayDiff = (strtotime($dateTo) - strtotime($dateFrom)) / 86400 + 1;
                $googleClientInfo['forecast'] = floor($googleClientInfo['balance'] / (($googleClientInfo['cost'] / 1000000) / $dayDiff));
                //$googleClientInfo['LEADS'] = self::getLeads($userName, $sessionDate);
                Session::put('googleClientInfo' . $userName . 'Date_' . $sessionDate['DateFrom'] . '_' . $sessionDate['DateTo'], $googleClientInfo);
            }
        } else {
            $googleClientInfo['error'] = 'У данного аккаунта не установлен googleLogin';
        }
//        echo'<pre>';
//        print_r($googleClientInfo);
//        echo'</pre>';
        return $googleClientInfo;
    }

    public function getSpendingLimit($googleLogin)
    {
        $cid = $googleLogin;
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(config('app.adsapi_php_path'))->build();
        $session = (new AdWordsSessionBuilder())
            ->fromFile(config('app.adsapi_php_path'))
            ->withOAuth2Credential($oAuth2Credential)
            ->withClientCustomerId($cid)
            ->build();
        $date[0] = date('Ymd', strtotime('2015-01-01'));
        $date[1] = date('Ymd', time());

        $budgetOrderService = (new AdWordsServices())->get($session, BudgetOrderService::class);

        $selector = new Selector();
        $selector->setFields(['CustomerId']);
        $o = $budgetOrderService->get($selector);
        $rs['budget'] = 0;
        $ids = [];
        $prs = $o->getEntries();
        if (isset($prs)) {


            if (is_array($o->getEntries())) {
                foreach ($o->getEntries() as $v) {
                    $getLastRequest = $v->getLastRequest();
                    if (isset($getLastRequest)) {
                        if ($v->getLastRequest()->getStatus() == 'APPROVED') {
                            //array_push($ids,intval($v->id));
                            $ids[intval($v->getId())] = str_replace('Asia/Shanghai', '', str_replace(' ', '', $v->getStartDateTime()));
                        }
                    }
                }
            }

            if (count($ids)) {
                arsort($ids);
                $ida = array_keys($ids);
                $id = $ida[0];
                foreach ($o->getEntries() as $v) {
                    if ($v->getId() == $id) {
                        $rs['bdate'] = substr($v->getStartDateTime(), 0, 8);
                        $rs['budget'] += $v->getSpendingLimit()->getMicroAmount() / 1000000;
                    }
                }
                $budget = sprintf('%01.2f', $rs['budget']);
            } else {
                $budget = 0;
            }
        } else {
            $budget = 0;
        }
        return $budget;
    }

    public function getLeads($userId, $dateRange)
    {
        $strDate = strtotime($dateRange['DateTo']) + 86400;
        $dateRange['DateTo'] = date('Y-m-d', $strDate);
        $leads = DB::table('leads_user')->where([['user_id', '=', $userId], ['source', 'like', '%google%']])->whereBetween('date', [$dateRange['DateFrom'], $dateRange['DateTo']])->get()->all();
        return $leads;
    }
}
