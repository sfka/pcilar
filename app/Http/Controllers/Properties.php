<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Properties extends Controller
{
    protected $propertiesDB = 'properties';

    public function index()
    {
        $properties = self::getPropertiesList();
        return view('admin.properties.index')->with([
            'properties' => $properties,
        ]);
    }

    public function getPropertiesList()
    {
        $properties = DB::select('select * from ' . $this->propertiesDB);
        return $properties;
    }

    public function updatePropertiesList(Request $request)
    {
        $arProps = [];
        foreach ($request->code as $key => $code) {
            $arProps[$key]['code'] = $code;
        }
        foreach ($request->name as $key => $name) {
            $arProps[$key]['name'] = $name;
        }
        foreach ($request->description as $key => $description) {
            $arProps[$key]['description'] = $description;
        }

        foreach ($arProps as $key => $prop) {
            $propSelect = DB::select('select * from ' . $this->propertiesDB . ' WHERE id = ' . $key);
            if (isset($propSelect[0])) {
                $arInsert = [];
                foreach ($prop as $keyTable => $tableCode) {
                    $arInsert[$keyTable] = $tableCode;
                }
                DB::table($this->propertiesDB)->where('id', $propSelect[0]->id)->update($arInsert);
            } else {
                $arInsert = [];
                foreach ($prop as $keyTable => $tableCode) {
                    $arInsert[$keyTable] = $tableCode;
                }
                DB::table($this->propertiesDB)->insert($arInsert);
            }
            unset($propSelect);
        }
        return view('admin.properties.update')->with([

        ]);
    }

    public function getPropertyDescription(Request $request)
    {
        $fields = $request->all();
        if (isset($fields['property'])) {
            foreach ($fields['property'] as $prop) {
                $arProp = DB::table($this->propertiesDB)->select('*')->where('code', '=', $prop)->first();
                if (isset($arProp)) {
                    $arResult[$prop] = (array)$arProp;
                } else {
                    $arResult[$prop]['error'] = 'Описание свойства не найдено';
                }
            }
        } else {
            $arResult['error'] = 'Неизвестная ошибка';
        }
        $arResult = json_encode($arResult);
//        return view('includes.property')->with([
//            'arResult' => $arResult
//        ]);
        return $arResult;
    }
}
