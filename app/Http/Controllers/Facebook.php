<?php

namespace App\Http\Controllers;

use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Facebook extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $url = 'https://graph.facebook.com/';
    private $client_id = '1451284475261549';
    private $client_secret = '3145bd2410f4ceee8efa5235e03154c4';
    private $vApi = 'v8.0';
    /*private $shortLivedToken = 'EAAUn73ZBdsm0BAJZASoYho1o8HwE2BFVEOCnlYDoJjQNxNHTdAJiyoLWTMBMDAXfwgZBBnCj133Qt1P2BicMoB14ZAykh2o3e58kzIIycKCbTqLdwb0iWZAk58EjlZCRo1xOXelRZCI1AqlsGNJHfVnkP0Ypk7sQ6oa62UB5XuK6BxLn47WHoZCtmsQp3MRS3BoZBrZAa6q3TV3lX1sFxrZCTDv';
    private $longLivedToken = 'EAAUn73ZBdsm0BABLxDauixq0nIce62kiKnoI9JKJCCkKjTPZBWILTw5UCdhmfH2eBtZC3BxwxT9hy3XZC6fI6xuTUv8cllcQVdm0ePHFo7tgDBjGuEjuwIvezZBDk4C4BqvYTCGnnhmme5DaMjlzWURcElR715wQxDobtpAdAMCAHLZBiK0c0x';*/
    private $shortLivedToken = 'EAAUn73ZBdsm0BAIRp7vdnqLtCql9eDy289HlAzttgkvanGSop4ZCjQcPRuSosTZA1bLCsFmc5ZA5DBWXXuzHOwnT2Nah3IURJvTc7VqqEmrWN9BQGZACcjVkQoeVmvL5CneYgsEHZBeDgtU1x9AT503wGXVBZBUqZBpNCE7ZAHH9BF3Noxz5EGqA17vOu2SWQ1lDu1ZAkgX8RXTwZDZD';
    private $longLivedToken = 'EAAUn73ZBdsm0BAMeJoJAaLFWxtyyp0zsY0gVdYiGpWoB0yCIeZCnRAOgTNB2ZCfPG0hZCH96pboGNFfgWy1mQgNZBpnJY0zZBpKyrEFN6x4PWZBuaTMQnPUTTpHJ26yDyqFytH0lHNMa4WGyz9jH9Jg0ZApNMhFRiWWhP2juTLyI6WpFM5rdx4rc';

    /**
     * @param $client_id
     * @param $client_secret
     * @return mixed
     */
    public function getAccessToken()
    {
        $arrContextOptions = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];
        $arAccessToken = json_decode(file_get_contents($this->url . $this->vApi . '/oauth/access_token?grant_type=fb_exchange_token&client_id=' . $this->client_id . '&client_secret=' . $this->client_secret . '&fb_exchange_token=' . $this->shortLivedToken, false, stream_context_create($arrContextOptions)));
        return $arAccessToken->access_token;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$countCalls = self::getCountCalls();
        return view('facebook')->with([

        ]);
    }

    /**
     * @return array
     */
    public function getCampaignsList(Request $request)
    {
        $fieldNames = ['id', 'name'];
        if (isset($request->fields)) {
            foreach ($request->fields as $field) {
                $fieldNamesNew[] = $field;
            }
            $fieldNames = array_merge($fieldNames, $fieldNamesNew);
            $request->session()->put('fieldNames', $fieldNames);
        } else {
            $fieldNames = $request->session()->get('fieldNames');
        }
        if ($request->input('next')) {
            $campaignsList = json_decode(self::getCurlRequest('get', $request->input('next')));
        } elseif ($request->input('previous')) {
            $campaignsList = json_decode(self::getCurlRequest('get', $request->input('previous')));
        } else {
            $accessToken = $this->longLivedToken;

            $facebookLogin = Auth::user()->facebookLogin;
            $effective_status = implode(',', $request->effective_status);
            if ($request->input('dateFrom') && $request->input('dateTo')) {
                $time_range = 'time_range={"since":"' . $request->input('dateFrom') . '","until":"' . $request->input('dateTo') . '"}';
            } else {
                $time_range = 'time_range={"since":"' . date('Y-m-d', strtotime('-7 days')) . '","until":"' . date('Y-m-d', strtotime('-1 days')) . '"}';
            }
            $campaignsList = json_decode(self::getCurlRequest('get', $this->url . $this->vApi . '/act_' . $facebookLogin . '/campaigns?limit=' . $request->input('entriesPerPage') . '&effective_status[]=' . $effective_status . '&' . $time_range . '&access_token=' . $accessToken . '&fields=' . implode($fieldNames, ',')));
        }
        $campaignsList = (array)$campaignsList;
        if (isset($campaignsList['paging']->next)) {
            $campaignsList['page']['next'] = $campaignsList['paging']->next;
        }
        if (isset($campaignsList['paging']->previous)) {
            $campaignsList['page']['previous'] = $campaignsList['paging']->previous;
        }
        if (isset($campaignsList['data'])) {
            foreach ($campaignsList['data'] as $key => $campaign) {
                $campaignsList['data'][$key] = (array)$campaign;
            }
        } elseif (isset($campaignsList['error'])) {
            $campaignsList['error'] = $campaignsList['error']->message;
        }

        return view('contents.facebook.campaigns')->with([
            'campaigns' => $campaignsList,
            'selectedFields' => $fieldNames,
        ]);
    }

    public function getCampaignInfo(Request $request)
    {
        if (!empty($request->input('campaignID'))) {
            $campaignID = $request->input('campaignID');
        }
        $fieldNames = [];
        if (isset($request->fields)) {
            foreach ($request->fields as $field) {
                $fieldNamesNew[] = $field;
            }
            $fieldNames = array_merge($fieldNames, $fieldNamesNew);
            $request->session()->put('fieldNames', $fieldNames);
        } else {
            $fieldNames = $request->session()->get('fieldNames');
        }
        if ($request->input('dateFrom') && $request->input('dateTo')) {
            $timeRange = '&time_range={"since":"' . $request->input('dateFrom') . '","until":"' . $request->input('dateTo') . '"}';
        } else {
            $timeRange = '&time_range={"since":"' . date('Y-m-d', strtotime('-7 days')) . '","until":"' . date('Y-m-d') . '"}';
        }
        $url = $this->url . $this->vApi . '/' . $campaignID . '/insights?fields=' . implode($fieldNames, ',') . $timeRange . '&access_token=' . $this->longLivedToken;
        $campaignInfo = json_decode(self::getCurlRequest('get', $url));
        $campaignInfo = $campaignInfo->data;
        foreach ($campaignInfo as $key => $campaign) {
            $campaignInfo[$key] = (array)$campaign;
        }
        if (empty($campaignInfo)) {
            $campaignInfo['error'] = 'Неизвестная ошибка, попробуйте позже';
        }
        return view('contents.facebook.campaign-info')->with([
            'campaignInfo' => $campaignInfo,
            'selectedFields' => $fieldNames,
        ]);
    }

    public function getClientInfo($dateFrom = NULL, $dateTo = NULL, $userID = NULL)
    {
        if ($userID == NULL) {
            $facebookLogin = Auth::user()->facebookLogin;
            $userName = Auth::user()->id;
        } else {
            $user = DB::table('users')->where('id', $userID)->first();
            $facebookLogin = $user->facebookLogin;
            $userName = $user->id;
        }
        if ($dateFrom == NULL) {
            $dateFrom = date('Y-m-d', strtotime('-7 days'));
        }
        if ($dateTo == NULL) {
            $dateTo = date('Y-m-d', strtotime('-1 days'));
        }
        if (isset($facebookLogin) && $facebookLogin != '') {
            if (Session::get('facebookClientInfo' . $userName . 'Date_' . $dateFrom . '_' . $dateTo)) {
                $facebookClientInfo = Session::get('facebookClientInfo' . $userName . 'Date_' . $dateFrom . '_' . $dateTo);
            } else {
                $fieldNames = ['clicks', 'conversions', 'cost_per_ad_click', 'cpc', 'ctr', 'cpm', 'impressions', 'cost_per_conversion', 'spend', 'unique_actions',
                    'cost_per_unique_action_type',];
                $fieldNamesBalance = ['balance'];

                $time_range = 'time_range={"since":"' . $dateFrom . '","until":"' . $dateTo . '"}';
                $url = $this->url . $this->vApi . '/act_' . $facebookLogin . '/insights?fields=' . implode(',', $fieldNames) . '&' . $time_range . '&access_token=' . $this->longLivedToken;
                $urlBalance = $this->url . $this->vApi . '/act_' . $facebookLogin . '?fields=' . implode(',', $fieldNamesBalance) . '&access_token=' . $this->longLivedToken;
                $facebookClientInfo = json_decode(self::getCurlRequest('get', $url));
                $facebookClientInfoBalance = json_decode(self::getCurlRequest('get', $urlBalance));
                $facebookClientInfoBalance = (array)$facebookClientInfoBalance;
                if (isset($facebookClientInfoBalance['balance']))
                    $facebookClientInfoBalance['balance'] = $facebookClientInfoBalance['balance'] / 100;
                $facebookClientInfo = (array)$facebookClientInfo;
                if (isset($facebookClientInfo['data'][0])) {
                    $facebookClientInfo = (array)$facebookClientInfo['data'][0];
                    $facebookClientInfo = array_merge($facebookClientInfo, $facebookClientInfoBalance);
                } else {
                    $facebookClientInfo['error'] = 'Не найдено данных за период: ' . date('d.m.Y', strtotime($dateFrom)) . ' - ' . date('d.m.Y', strtotime($dateTo));
                }
                if (isset($facebookClientInfo['unique_actions'])) {
                    foreach ($facebookClientInfo['unique_actions'] as $key => $uniqAction) {
                        $facebookClientInfo['SORT_ACTIONS'][$uniqAction->action_type]['COUNT'] = $uniqAction->value;
                    }
                }
                if (isset($facebookClientInfo['cost_per_unique_action_type'])) {
                    foreach ($facebookClientInfo['cost_per_unique_action_type'] as $key => $uniqAction) {
                        $facebookClientInfo['SORT_ACTIONS'][$uniqAction->action_type]['COST'] = $uniqAction->value;
                    }
                }
                $dayDiff = (strtotime($dateTo) - strtotime($dateFrom)) / 86400 + 1;
                if (isset($facebookClientInfo['balance'])) {
                    $facebookClientInfo['forecast'] = floor($facebookClientInfo['balance'] / ($facebookClientInfo['spend'] / $dayDiff));
                }
                if (isset($facebookClientInfo['clicks']))
                    $facebookClientInfo['Ctr'] = round(($facebookClientInfo['clicks'] / $facebookClientInfo['impressions']) * 100, 1);
                Session::put('facebookClientInfo' . $userName . 'Date_' . $dateFrom . '_' . $dateTo, $facebookClientInfo);
            }
        } else {
            $facebookClientInfo['error'] = 'У данного аккаунта не установлен facebookLogin';
        }
        return $facebookClientInfo;
    }

    public function getClientInfoPage()
    {
        $facebookClientInfo = $this->getClientInfo();
        return view('contents.facebook.client-info')->with([
            'arResult' => $facebookClientInfo,
        ]);
    }

    public function getCurlRequest($type, $url)
    {
        if ($url != '') {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            if ($type == 'post') {
                curl_setopt($curl, CURLOPT_POST, true);
            }
            $res = curl_exec($curl);
            curl_close($curl);
            return $res;
        }
        return false;
    }
}
