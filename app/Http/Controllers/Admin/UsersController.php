<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use Gate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.users.index')->with([
            'users' => $users,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (Gate::denies('edit-users')) {
            return redirect(route('admin.users.index'));
        }
        $roles = Role::all();
        return view('admin.users.edit')->with([
            'user' => $user,
            'roles' => $roles,
        ]);
    }

    /**
     * Create the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {

        $users = User::all();
        if($request->email) {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->googleLogin = $request->googleLogin;
            $user->yandexLogin = $request->yandexLogin;
            $user->yandexCounterId = $request->yandexCounterId;
            $user->facebookLogin = $request->facebookLogin;
            $user->callTrackLogin = $request->callTrackLogin;
            $user->callTrackPassword = $request->callTrackPassword;
            $user->callTrackProject = $request->callTrackProject;
            $user->password = Hash::make($request->password);
            $user->save();
        }
        $role = Role::select('id')->where('name', 'user')->first();

        $user->roles()->attach($role);
//        return view('admin.users.index')->with([
//            'users' => $users,
//        ]);
        return redirect()->route('admin.users.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->roles()->sync($request->roles);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->googleLogin = $request->googleLogin;
        $user->yandexLogin = $request->yandexLogin;
        $user->yandexCounterId = $request->yandexCounterId;
        $user->facebookLogin = $request->facebookLogin;
        $user->callTrackLogin = $request->callTrackLogin;
        $user->callTrackPassword = $request->callTrackPassword;
        $user->callTrackProject = $request->callTrackProject;
        $user->save();

        if ($user->save()) {
            $request->session()->flash('success', $user->name . ' has been updated');
        } else {
            $request->session()->flash('error', 'There was an error updating the user');
        }


        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Gate::denies('delete-users')) {
            return redirect(route('admin.users.index'));
        }

        $user->roles()->detach();
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
