<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CallTracking extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return mixed
     */
    public static function getToken($login = NULL, $pass = NULL)
    {
        if ($login == NULL && $pass == NULL) {
            $callTrackLogin = Auth::user()->callTrackLogin;
            $callTrackPass = Auth::user()->callTrackPassword;
        } else {
            $callTrackLogin = $login;
            $callTrackPass = $pass;
        }
        $arrContextOptions = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];
        $callTrackToken = json_decode(file_get_contents('https://calltracking.ru/api/login.php?account_type=calltracking&login=' . $callTrackLogin . '&password=' . $callTrackPass . '&service=analytics', false, stream_context_create($arrContextOptions)));
        $arCallTrackToken = (array)$callTrackToken;
        return $arCallTrackToken['data'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$countCalls = self::getCountCalls();
        return view('calltrack');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getCountCalls(Request $request)
    {
        if ($request->input('dateFrom') && $request->input('dateTo')) {
            $dateRange = 'start-date=' . $request->input('dateFrom') . '&end-date=' . $request->input('dateTo');
        } else {
            $dateRange = 'start-date=' . date('Y-m-d', strtotime('-7 days')) . '&end-date=' . date('Y-m-d', strtotime('-1 days'));
        }
        $arrContextOptions = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];
        if ($request->input('start')) {
            $start = $request->input('start');
        } else {
            $start = 0;
        }
        $limit = $request->input('entriesPerPage');
        $callTrackProject = Auth::user()->callTrackProject;
        $token = self::getToken();
        $calltrackClientInfo = (array)json_decode(file_get_contents('https://calltracking.ru/api/get_data.php?project=' . $callTrackProject . '&auth=' . $token . '&dimensions=ct:date,ct:caller,ct:status&metrics=ct:calls&sort=-ct:datetime&max-results=' . $limit . '&start-index=' . $start . '&' . $dateRange, false, stream_context_create($arrContextOptions)));

        if (isset($calltrackClientInfo['data'])) {
            $countCallsList = json_decode(json_encode($calltrackClientInfo['data']),TRUE);
        }
        foreach($countCallsList as $keyCt => $ctField) {
            $countCallsList[$keyCt]['TOTAL_A'] = 0;
            $countCallsList[$keyCt]['TOTAL_N'] = 0;
            $countCallsList[$keyCt]['TOTAL_B'] = 0;
            $countCallsList[$keyCt]['TOTAL_F'] = 0;
            foreach($ctField as $key => $field) {
                if (isset($field['ANSWERED'])) $countCallsList[$keyCt]['TOTAL_A'] += $field['ANSWERED']['ct:calls'];
                if (isset($field['NO ANSWER'])) $countCallsList[$keyCt]['TOTAL_N'] += $field['NO ANSWER']['ct:calls'];
                if (isset($field['BUSY'])) $countCallsList[$keyCt]['TOTAL_B'] += $field['BUSY']['ct:calls'];
                if (isset($field['FAILED']))  $countCallsList[$keyCt]['TOTAL_F'] += $field['FAILED']['ct:calls'];
            }
        }
        $days = (strtotime($request->input('dateTo')) - strtotime($request->input('dateFrom'))) / (60 * 60 * 24);
        $countRows = 0;
        foreach($countCallsList as $day) {
            $countRows += count($day);
        }
        $paging = [
            'Next' => ($start + $request->input('entriesPerPage') > $countRows ? 'N' : $start + $request->input('entriesPerPage')),
            'Prev' => $start == 0 ? 'N' : $start - $request->input('entriesPerPage'),
            'Limit' => $request->input('entriesPerPage'),
            'dateFrom' => $request->input('dateFrom'),
            'dateTo' => $request->input('dateTo'),
        ];
        return view('contents.calltrack.count-calls')->with([
            'countCalls' => $countCallsList,
            'paging' => $paging,
        ]);
    }

    public static function getClientInfo($dateFrom = NULL, $dateTo = NULL, $userID = NULL)
    {
        if ($userID == NULL) {
            $token = self::getToken();
            $callTrackProject = Auth::user()->callTrackProject;
            $userName = Auth::user()->id;
        } else {
            $user = DB::table('users')->where('id', $userID)->first();
            $token = self::getToken($user->callTrackLogin, $user->callTrackPassword);
            $callTrackProject = $user->callTrackProject;
            $userName = $user->id;
        }
        if ($dateFrom == NULL) {
            $dateFrom = date('Y-m-d', strtotime('-7 days'));
        }
        if ($dateTo == NULL) {
            $dateTo = date('Y-m-d', strtotime('-1 days'));
        }
        if (isset($callTrackProject) && $callTrackProject != '') {
            if (Session::get('calltrackClientInfo' . $userName . 'Date_' . $dateFrom . '_' . $dateTo)) {
                $calltrackClientInfo = Session::get('calltrackClientInfo' . $userName . 'Date_' . $dateFrom . '_' . $dateTo);
            } else {
                $arrContextOptions = [
                    "ssl" => [
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                    ],
                ];
                $dateRange = 'start-date=' . $dateFrom . '&end-date=' . $dateTo;

                $calltrackClientInfo = (array)json_decode(file_get_contents('https://calltracking.ru/api/get_data.php?project=' . $callTrackProject . '&auth=' . $token . '&dimensions=ct:date,ct:caller,ct:status&metrics=ct:calls&sort=-ct:datetime&max-results=2000&start-index=0&' . $dateRange, false, stream_context_create($arrContextOptions)));
                Session::put('calltrackClientInfo' . $userName . 'Date_' . $dateFrom . '_' . $dateTo, $calltrackClientInfo);
            }

            if (isset($calltrackClientInfo['data'])) {
                $countCallsList = json_decode(json_encode($calltrackClientInfo['data']), TRUE);
            }
            foreach ($countCallsList as $keyCt => $ctField) {
                $countCallsList[$keyCt]['TOTAL_A'] = 0;
                $countCallsList[$keyCt]['TOTAL_N'] = 0;
                $countCallsList[$keyCt]['TOTAL_B'] = 0;
                $countCallsList[$keyCt]['TOTAL_F'] = 0;
                foreach ($ctField as $key => $field) {
                    if (isset($field['ANSWERED'])) $countCallsList[$keyCt]['TOTAL_A'] += $field['ANSWERED']['ct:calls'];
                    if (isset($field['NO ANSWER'])) $countCallsList[$keyCt]['TOTAL_N'] += $field['NO ANSWER']['ct:calls'];
                    if (isset($field['BUSY'])) $countCallsList[$keyCt]['TOTAL_B'] += $field['BUSY']['ct:calls'];
                    if (isset($field['FAILED'])) $countCallsList[$keyCt]['TOTAL_F'] += $field['FAILED']['ct:calls'];
                }
            }
        } else {
            $countCallsList['error'] = 'У данного аккаунта не установлен callTrackLogin';
        }

        return $countCallsList;

    }

    public function getClientInfoPage()
    {
        $countCallsList = $this->getClientInfo();
        return view('contents.calltrack.client-info')->with([
            'arResult' => $countCallsList,
        ]);
    }
}
