@extends('layouts.app')

@section('content')
    <h4>{{ __('Панель') }}</h4>
    @if ( session('status') )
        <p>
            {{ session('status') }}
        </p>
    @endif
    <p>{{ __('Вы вошли в систему!') }}</p>
@endsection
