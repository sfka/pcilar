@extends('layouts.app')
@section('content')
    @if(isset($arResult['USERS']))
        <form action="{{ url('leads/load') }}" method="POST" class="ajax__form file_form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col s12">
                    Загрузка новых лидов
                </div>
            </div>
            <div class="row">
                <div class="col m6 s12 input-field">
                    <select name="user_name">
                        @foreach($arResult['USERS'] as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    <label>Идентификатор клиента</label>
                </div>
            </div>
            <div class="row">
                <div class="col m6 s12 file-field input-field">
                    <div class="btn">
                        <span>Загрузить</span>
                        <input type="file" name="venyoo_file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Файл venyoo в формате csv">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col m6 s12 file-field input-field">
                    <div class="btn">
                        <span>Загрузить</span>
                        <input type="file" name="site_file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Файл site в формате csv">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col m6 s12 file-field input-field">
                    <div class="btn">
                        <span>Загрузить</span>
                        <input type="file" name="calltrack_file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Файл calltrack в формате csv">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col m6 s12 input-field">
                    <button class="btn waves-effect waves-light" type="submit">
                        Отправить
                    </button>
                </div>
            </div>
        </form>
    @endif
    <form action="{{ url('leads/') }}" method="POST" class="ajax__form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col s12">
                Список лидов
            </div>
        </div>
        @if(isset($arResult['USERS']))
            <div class="row">
                <div class="col m6 s12 input-field">
                    <select name="user_name">
                        @foreach($arResult['USERS'] as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    <label>Идентификатор клиента</label>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col m6 s12 input-field">
                <input id="dateFrom" class="datepicker" type="text" name="dateFrom">
                <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
            </div>
            <div class="col m6 s12 input-field">
                <input id="dateTo" class="datepicker" type="text" name="dateTo">
                <label for="dateTo">Дата конца: dd / mm / yyyy</label>
            </div>
        </div>
        <div class="row">
            <div class="col m6 s12 input-field">
                <button class="btn waves-effect waves-light" type="submit">
                    Получить список
                </button>
            </div>
        </div>
    </form>
    <div class="content_form">
        @if(isset($arResult['LEADS']['LEADS_USER']))
            @if(isset($arResult['Date']))
                <h4>Лиды за период: {{ date('d.m.Y', strtotime($arResult['Date']['DateFrom'])) }}
                    - {{ date('d.m.Y', strtotime($arResult['Date']['DateTo'])) }}</h4>
            @endif
            <a href="/tmp_files/{{ $arResult['LEADS']['FILE']  }}" class="btn waves-effect waves-light">Выгрузить лиды в CSV</a>
            <div class="table_wrap">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Дата</th>
                        @if(isset($arResult['USERS']))
                            <th scope="col">Источник</th>
                            <th scope="col">Канал</th>
                            <th scope="col">Ключевое слово</th>
                            <th scope="col">Файл</th>
                        @endif
                        <th><span class="tooltipped" data-position="bottom" data-tooltip="Посетитель заинтересован в покупке товара / услуги.">Валидный</span></th>
                        <th>Комментарий</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?$leadIDs = [] ?>
                    @foreach($arResult['LEADS']['LEADS_USER'] as $key => $lead)
                        <?$leadIDs[] = $lead->id ?>
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td><span class="nowrap">{{ $lead->phone }}</span></td>
                            <td>{{ date('d.m.Y H:i:s', strtotime($lead->date)) }}</td>
                            @if(isset($arResult['USERS']))
                                <td>{{ $lead->source }}</td>
                                <td>{{ $lead->channel }}</td>
                                <td>{{ $lead->word }}</td>
                                <td>{{ $lead->file }}</td>
                            @endif
                            <td class="tac">
                                <label class="valid_label">
                                    <input name="valid" type="checkbox" data-id="{{ $lead->id }}" value="Y"
                                           @if($lead->valid == 'Y') checked="checked"
                                           @endif @if(isset($arResult['USERS'])) disabled
                                           @endif class="filled-in ajax__lead_update">
                                    <span><code>Да</code></span>
                                </label>
                                <label class="valid_label">
                                    <input name="valid" type="checkbox" data-id="{{ $lead->id }}" value="N"
                                           @if($lead->valid == 'N') checked="checked"
                                           @endif @if(isset($arResult['USERS'])) disabled
                                           @endif class="filled-in ajax__lead_update">
                                    <span><code>Нет</code></span>
                                </label>
                            </td>
                            <td>
                                <textarea class="ajax__lead_update" data-id="{{ $lead->id }}" name="comment" id=""
                                          cols="30" rows="10"
                                          @if(isset($arResult['USERS'])) disabled @endif>{{ $lead->comment }}</textarea>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
