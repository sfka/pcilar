@if(isset($arResult['NEW_LEADS']))
    <div class="table_wrap">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Телефон</th>
                <th scope="col">Дата</th>
                <th scope="col">Источник</th>
                <th scope="col">Канал</th>
                <th scope="col">Ключевое слово</th>
                <th scope="col">Файл</th>
            </tr>
            </thead>
            <tbody>
            @foreach($arResult['NEW_LEADS'] as $key => $lead)
                <tr>
                    <td>{{ $key }}</td>
                    <td>{{ $lead['phone'] }}</td>
                    <td>{{ date('d.m.Y H:i:s', strtotime($lead['date'])) }}</td>
                    <td>{{ $lead['source'] }}</td>
                    <td>{{ $lead['channel'] }}</td>
                    <td>{{ $lead['word'] }}</td>
                    <td>{{ $lead['file'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @if(isset($arResult['success_message']))
        <p>{{ $arResult['success_message'] }}</p>
    @endif
@elseif(isset($arResult['error']))
    {{ $arResult['error'] }}
@endif
