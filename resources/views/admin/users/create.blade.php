@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12">
            <h4>Создать пользователя</h4>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <form action="{{ route('admin.users.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="name">Имя</label>
                        <input id="name" type="text" class="validate @error('name') invalid @enderror" name="name" value="" required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="email">E-mail</label>
                        <input id="email" type="email" class="validate @error('email') invalid @enderror" name="email" value="" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="password">Пароль</label>
                        <input id="password" type="text" class="validate @error('password') invalid @enderror" name="password" value="">
                        <div class="btn-group btn-group_offset-top-small">
                            <span class="btn waves-effect waves-light generate-pass" onclick="generateText('password')">
                                Сгенерировать
                            </span>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="googleLogin">Google Login</label>
                        <input id="googleLogin" type="text" class="validate @error('googleLogin') invalid @enderror" name="googleLogin" value="">

                        @error('googleLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="yandexLogin">Yandex Login</label>
                        <input id="yandexLogin" type="text" class="validate @error('yandexLogin') invalid @enderror" name="yandexLogin" value="">

                        @error('yandexLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="yandexCounterId">Yandex Counter</label>
                        <input id="yandexCounterId" type="text" class="validate @error('yandexCounterId') invalid @enderror" name="yandexCounterId" value="">

                        @error('yandexCounterId')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="facebookLogin">Facebook Login</label>
                        <input id="facebookLogin" type="text" class="validate @error('facebookLogin') invalid @enderror" name="facebookLogin" value="">
                        @error('facebookLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="callTrackLogin">CallTracking Login</label>
                        <input id="callTrackLogin" type="text" class="validate @error('callTrackLogin') invalid @enderror" name="callTrackLogin" value="">

                        @error('callTrackLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="callTrackPassword">CallTracking Password</label>
                        <input id="callTrackPassword" type="text" class="validate @error('callTrackPassword') invalid @enderror" name="callTrackPassword" value="">

                        @error('callTrackPassword')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="callTrackProject">CallTracking Project</label>
                        <input id="callTrackProject" type="text" class="validate @error('callTrackProject') invalid @enderror" name="callTrackProject" value="">

                        @error('callTrackProject')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <button type="submit" class="btn btn-large waves-effect waves-light">Создать</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
