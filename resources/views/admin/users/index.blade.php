@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12">
            <h4>Пользователи</h4>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <div class="table_wrap">
                <table class="table">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Имя</td>
                        <td>E-mail</td>
                        <td>Роль</td>
                        <td>Действия</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $users as $user)
                        <tr>
                            <th>{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ implode(', ', $user->roles()->get()->pluck('name')->toArray()) }}</td>
                            <td>
                                <div class="btn-group">
                                    @can('edit-users')
                                        <a class="btn btn-primary" href="{{ route('admin.users.edit', $user->id) }}">Изменить</a>
                                    @endcan
                                    @can('delete-users')
                                        <form action="{{ route('admin.users.destroy', $user) }}" method="POST">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="waves-effect waves-light btn">Удалить</button>
                                        </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="btn-group">
                <a href="{{ route('admin.users.create') }}" class="waves-effect waves-light btn btn-large">Добавить
                    нового пользователя</a>
            </div>
            <div class="stats-data">
            </div>
        </div>
    </div>

@endsection
