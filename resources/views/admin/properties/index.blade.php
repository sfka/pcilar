@extends('layouts.app')

@section('content')
    @if(isset($properties))
        <h3>Свойства</h3>
        <form action="{{ url('properties/update') }}" method="POST" class="ajax__form">
            {{ csrf_field() }}
            <div class="table_wrap">
                <table class="table">
                    @foreach($properties as $key => $prop)
                        <tr data-id="{{ $key+1 }}">
                            <td>{{ $key+1 }}</td>
                            <td>
                                <div class="input-field">
                                    <input type="text" name="code[{{ $prop->id }}]" value="{{ $prop->code }}">
                                    <label>Код</label>
                                </div>
                            </td>
                            <td>
                                <div class="input-field">
                                    <input type="text" name="name[{{ $prop->id }}]" value="{{ $prop->name }}">
                                    <label>Имя</label>
                                </div>
                            </td>
                            <td>
                                <div class="input-field">
                                    <input type="text" name="description[{{ $prop->id }}]"
                                           value="{{ $prop->description }}">
                                    <label>Описание</label>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="btn-group">
                <a class="waves-effect waves-light btn btn-large" href="javascript:void(0)"
                   onclick="addNewPropRow($(this))">Добавить новое свойство</a>
                <button type="submit" class="waves-effect waves-light btn btn-large">Обновить</button>
            </div>
        </form>
    @endif
    <div class="content_form"></div>
@endsection
