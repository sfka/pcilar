@extends('layouts.app')
@section('content')
    <form action="{{ url('compare/list') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
        <div class="row">
            <div class="col m6 s12 input-field">
                <select name="platform" >
                    @if(isset(Auth::user()->yandexLogin))<option>Yandex</option>@endif
                    @if(isset(Auth::user()->googleLogin))<option>Google</option>@endif
                    @if(isset(Auth::user()->facebookLogin))<option>Facebook</option>@endif
                    @if(isset(Auth::user()->callTrackLogin))<option>CallTrack</option>@endif
                </select>
                <label>Социальные сети:</label>
            </div>
        </div>
        <div class="row">
            <div class="col m2 s12 input-field">
                <p>Период 1</p>
            </div>
            <div class="col m5 s12 input-field">
                <input id="dateFrom" class="datepicker" type="text" name="dateFrom1">
                <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
            </div>
            <div class="col m5 s12 input-field">
                <input id="dateTo" class="datepicker" type="text" name="dateTo1">
                <label for="dateTo">Дата конца: dd / mm / yyyy</label>
            </div>
        </div>
        <div class="row">
            <div class="col m2 s12 input-field">
                <p>Период 2</p>
            </div>
            <div class="col m5 s12 input-field">
                <input id="dateFrom2" class="datepicker" type="text" name="dateFrom2">
                <label for="dateFrom2">Дата начала: dd / mm / yyyy</label>
            </div>
            <div class="col m5 s12 input-field">
                <input id="dateTo2" class="datepicker" type="text" name="dateTo2">
                <label for="dateTo2">Дата конца: dd / mm / yyyy</label>
            </div>
        </div>
        <div class="row">
            <div class="col m6 s12 input-field">
                <button class="btn btn-large waves-effect waves-light" type="submit">
                    Сравнить
                </button>
            </div>
        </div>
    </form>
    <div class="dynamic-content content_form"></div>
@endsection
