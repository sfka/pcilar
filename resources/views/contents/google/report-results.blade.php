<div class="container-fluid mt-2">
    <div class="table_wrap">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                @foreach ($selectedFields as $field)
                    <th scope="col">{{ $field }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @forelse ($reportResults as $index => $row)
                <tr>
                    <th scope="row">{{ $index + 1 }}</th>
                    @foreach ($row['@attributes'] as $field => $cellValue)
                        @if($field == 'budget' || $field == 'cost')
                            <td>{{ round($cellValue / 1000000, 2) }}</td>
                        @else
                            <td> {{ $cellValue }}</td>
                        @endif
                    @endforeach
                </tr>
            @empty
                <tr class="text-center">
                    <td colspan="{{ count($selectedFields) + 1 }}">
                        <strong>No data for this query.</strong></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>

{{ $reportResults->links() }}
