<div class="row">
    @if(isset($arResult['FACEBOOK_INFO']))
        @foreach($arResult['FACEBOOK_INFO'] as $keyFb => $fb)
            <div class="col m6 s12">
                <div class="dynamic-content__inner">
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <b> {{ $arResult['Date'][$keyFb]['DateFrom'] }}
                                - {{ $arResult['Date'][$keyFb]['DateTo'] }}</b>
                        </li>
                        @if(isset($fb['error']))
                            <li class="collection-item">
                                {{ $fb['error'] }}
                            </li>
                        @else
                            <li class="collection-header">
                                <b>Бюджет</b>
                            </li>
                            @if(isset($fb['spend']))
                                <li class="collection-item">
                                    Расход: {{ number_format($fb['spend'], 2, '.', ' ') }} ₽
                                    @if($keyFb < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['FACEBOOK_INFO'][$keyFb]['spend'], $arResult['FACEBOOK_INFO'][$keyFb + 1]['spend']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($fb['balance']))
                                <li class="collection-item">
                                    Сумма остатка: {{ number_format($fb['balance'], 2, '.', ' ') }} ₽
                                </li>
                            @endif
                            @if(isset($fb['forecast']))
                                <li class="collection-item">
                                    Прогноз бюджета: {{ $fb['forecast'] }}
                                </li>
                            @endif
                            <li class="collection-header">
                                <b>Статистика</b>
                            </li>
                            @if(isset($fb['cpc']))
                                <li class="collection-item">
                                    Стоимость клика: {{ number_format($fb['cpc'], 2, '.', ' ') }} ₽
                                    @if($keyFb < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['FACEBOOK_INFO'][$keyFb]['cpc'], $arResult['FACEBOOK_INFO'][$keyFb + 1]['cpc']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($fb['clicks']))
                                <li class="collection-item">
                                    Количество кликов: {{ $fb['clicks'] }}
                                    @if($keyFb < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['FACEBOOK_INFO'][$keyFb]['clicks'], $arResult['FACEBOOK_INFO'][$keyFb + 1]['clicks']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($fb['impressions']))
                                <li class="collection-item">
                                    Количество показов: {{ $fb['impressions'] }}
                                    @if($keyFb < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['FACEBOOK_INFO'][$keyFb]['impressions'], $arResult['FACEBOOK_INFO'][$keyFb + 1]['impressions']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($fb['Ctr']))
                                <li class="collection-item">
                                    Ctr: {{ $fb['Ctr'] }} %
                                </li>
                            @endif
                            @if(isset($fb['SORT_ACTIONS']))
                                <li class="collection-header">
                                    <b>Лиды</b>
                                </li>
                                @foreach($fb['SORT_ACTIONS'] as $key => $action)
                                    <li class="collection-item">{{ $key }} : {{ $action['COUNT'] }}
                                        @if(isset($action['COST']))
                                            <b>Цена:</b> {{ number_format(round($action['COST'], 2), 2, '.', ' ') }} ₽
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        @endforeach
    @elseif(isset($arResult['YANDEX_INFO']))
        @foreach($arResult['YANDEX_INFO'] as $keyYa => $ya)
            <div class="col m6 s12">
                <div class="dynamic-content__inner">
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <b> {{ $arResult['Date'][$keyYa]['DateFrom'] }}
                                - {{ $arResult['Date'][$keyYa]['DateTo'] }}</b>
                        </li>
                        @if(isset($ya['error']))
                            <li class="collection-item">
                                {{ $ya['error'] }}
                            </li>
                        @else
                            <li class="collection-header">
                                <b>Бюджет</b>
                            </li>
                            @if(isset($ya['Cost']))
                                <li class="collection-item">
                                    Общий расход: {{ number_format(round($ya['Cost'], 2), 2, '.', ' ') }} ₽
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['Cost'], $arResult['YANDEX_INFO'][$keyYa + 1]['Cost']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($ya['Balance']))
                                <li class="collection-item">
                                    Сумма остатка: {{ number_format($ya['Balance'], 2, '.', ' ') }} ₽
                                </li>
                            @endif
                            @if(isset($ya['forecast']))
                                <li class="collection-item">
                                    Прогноз бюджета: {{ $ya['forecast'] }}
                                </li>
                            @endif
                            <li class="collection-header">
                                <b>Статистика</b>
                            </li>
                            @if(isset($ya['OrderCost']))
                                <li class="collection-item">
                                    Стоимость заявки: {{ number_format($ya['OrderCost'], 2, '.', ' ') }} ₽
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['OrderCost'], $arResult['YANDEX_INFO'][$keyYa + 1]['OrderCost']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($ya['AvgCpc']))
                                <li class="collection-item">
                                    Стоимость клика: {{ number_format($ya['AvgCpc'], 2, '.', ' ') }} ₽
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['AvgCpc'], $arResult['YANDEX_INFO'][$keyYa + 1]['AvgCpc']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($ya['Clicks']))
                                <li class="collection-item">
                                    Количество кликов: {{ $ya['Clicks'] }}
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['Clicks'], $arResult['YANDEX_INFO'][$keyYa + 1]['Clicks']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($ya['Impressions']))
                                <li class="collection-item">
                                    Количество показов: {{ $ya['Impressions'] }}
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['Impressions'], $arResult['YANDEX_INFO'][$keyYa + 1]['Impressions']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($ya['Ctr']))
                                <li class="collection-item">
                                    Ctr: {{ $ya['Ctr'] }} %
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['Ctr'], $arResult['YANDEX_INFO'][$keyYa + 1]['Ctr']) !!}
                                    @endif
                                </li>
                            @endif
                            <li class="collection-header">
                                <b>Лиды</b>
                            </li>
                            @if(isset($ya['metric']['totalGoalsVisits']))
                                <li class="collection-item">
                                    Количество лидов: {{ $ya['metric']['totalGoalsVisits'] }}
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['metric']['totalGoalsVisits'], $arResult['YANDEX_INFO'][$keyYa + 1]['metric']['totalGoalsVisits']) !!}
                                    @endif
                                </li>
                            @endif
                            @if(isset($ya['metric']['visits']))
                                @foreach($ya['goals'] as $arGoal)
                                    <li class="collection-item">
                                        {{ $arGoal['name'] }}: {{ $arGoal['visits'] }}
                                    </li>
                                @endforeach
                            @endif
                            @if(isset($ya['goalConversion']))
                                <li class="collection-item">
                                    Конверсия:{{ $ya['goalConversion']  }}
                                    @if($keyYa < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['YANDEX_INFO'][$keyYa]['goalConversion'], $arResult['YANDEX_INFO'][$keyYa + 1]['goalConversion']) !!}
                                    @endif
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        @endforeach
    @elseif(isset($arResult['CALLTRACK_INFO']))
        @foreach($arResult['CALLTRACK_INFO'] as $keyCt => $ct)
            <div class="col m6 s12">
                <div class="dynamic-content__inner">
                    <ul class="collection with-header">
                        <?$itogCt = 0; ?>
                        @foreach($ct as $keyCt => $ctField)
                            <li class="collection-header tac">
                                <b>{{ date('d.m.Y', strtotime($keyCt)) }}</b>
                            </li>
                            <table>
                                @foreach($ctField as $key => $field)
                                    @if($key != 'TOTAL_A' && $key != 'TOTAL_N' && $key != 'TOTAL_B' && $key != 'TOTAL_F')
                                        <tr>
                                            <td>{{ $key }}</td>
                                            <td class="tac">
                                                @if(isset($field['ANSWERED']))
                                                    отвеченных: {{ $field['ANSWERED']['ct:calls'] }}
                                                    <?$itogCt += $field['ANSWERED']['ct:calls'] ?>
                                                @endif
                                                @if(isset($field['NO ANSWER']))
                                                    неотвеченных: {{ $field['NO ANSWER']['ct:calls'] }}
                                                    <?$itogCt += $field['NO ANSWER']['ct:calls'] ?>
                                                @endif
                                                @if(isset($field['BUSY']))
                                                    занято во время звонка: {{ $field['BUSY']['ct:calls'] }}
                                                    <?$itogCt += $field['BUSY']['ct:calls'] ?>
                                                @endif
                                                @if(isset($field['FAILED ']))
                                                    ошибка оборудования: {{ $field['FAILED']['ct:calls'] }}
                                                    <?$itogCt += $field['FAILED']['ct:calls'] ?>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        @endforeach
                        <li class="collection-item">
                            Общее количество за выбранный период: {{ $itogCt }}
                        </li>
                    </ul>
                </div>
            </div>
        @endforeach
    @elseif(isset($arResult['GOOGLE_INFO']))
        @foreach($arResult['GOOGLE_INFO'] as $keyGo => $go)
            <div class="col m6 s12">
                <div class="dynamic-content__inner">
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <b> {{ $arResult['Date'][$keyGo]['DateFrom'] }}
                                - {{ $arResult['Date'][$keyGo]['DateTo'] }}</b>
                        </li>
                        <li class="collection-header">
                            <b>Бюджет</b>
                        </li>
                        @if(isset($go['cost']))
                            <li class="collection-item">
                                Общий расход: {{ number_format(round($go['cost'] / 1000000, 2), 2, '.', ' ') }} ₽
                                @if($keyGo < 2)
                                    {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['cost'], $arResult['GOOGLE_INFO'][$keyGo + 1]['cost']) !!}
                                @endif
                            </li>
                        @endif
                        @if(isset($go['balance']))
                            <li class="collection-item">
                                Примерный остаток: {{ number_format(round($go['balance'], 2), 2, '.', ' ') }} ₽
                            </li>
                        @endif
                        @if(isset($go['forecast']))
                            <li class="collection-item">
                                Прогноз бюджета (дней): {{ $go['forecast'] }}
                            </li>
                        @endif
                        <li class="collection-header">
                            <b>Статистика</b>
                        </li>
                        @if(isset($go['avgCPC']))
                            <li class="collection-item">
                                Стоимость клика: {{ number_format(round($go['avgCPC'] / 1000000, 2), 2, '.', ' ') }} ₽
                                @if($keyGo < 2)
                                    {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['avgCPC'], $arResult['GOOGLE_INFO'][$keyGo + 1]['avgCPC']) !!}
                                @endif
                            </li>
                        @endif
                        @if(isset($go['clicks']))
                            <li class="collection-item">
                                Количество кликов: {{ $go['clicks'] }}
                                @if($keyGo < 2)
                                    {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['clicks'], $arResult['GOOGLE_INFO'][$keyGo + 1]['clicks']) !!}
                                @endif
                            </li>
                        @endif
                        @if(isset($go['impressions']))
                            <li class="collection-item">
                                Количество показов: {{ $go['impressions'] }}
                                @if($keyGo < 2)
                                    {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['impressions'], $arResult['GOOGLE_INFO'][$keyGo + 1]['impressions']) !!}
                                @endif
                            </li>
                        @endif
                        @if(isset($go['Ctr']))
                            <li class="collection-item">
                                Ctr: {{ $go['Ctr'] }} %
                                @if($keyGo < 2)
                                    {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['Ctr'], $arResult['GOOGLE_INFO'][$keyGo + 1]['Ctr']) !!}
                                @endif
                            </li>
                        @endif
                        @if(isset($go['conversions']) && $go['conversions'] > 0)
                            <li class="collection-header">
                                <b>Лиды</b>
                            </li>
                            <li class="collection-item">
                                Конверсия: {{ round($go['conversions']) }}
                                @if($keyGo < 2)
                                    {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['conversions'], $arResult['GOOGLE_INFO'][$keyGo + 1]['conversions']) !!}
                                @endif
                            </li>
                            @if(isset($go['costConv']))
                                <li class="collection-item">
                                    Стоимость заявки: {{ number_format(round($go['costConv'] / 1000000, 2), 2, '.', ' ') }} ₽
                                    @if($keyGo < 2)
                                        {!! \App\Http\Controllers\Dashboard::getCompareNumber($arResult['GOOGLE_INFO'][$keyGo]['costConv'], $arResult['GOOGLE_INFO'][$keyGo + 1]['costConv']) !!}
                                    @endif
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        @endforeach
    @endif
</div>
