<div class="row">
    <div class="col-md-12">
        <h3>CallTrack</h3>
    </div>
    @if (isset($arResult))
        <div class="table_wrap">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th>Date</th>
                    <th>Calls</th>
                </tr>
                </thead>
                <tbody>
                <? $count = 1  ?>
                @foreach ($arResult as $key => $field)
                    <tr>
                        <td><?= $count++  ?></td>
                        <td>{{ $key }}</td>
                        <td>{{ $arResult[$key]['ct:calls'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif
</div>
