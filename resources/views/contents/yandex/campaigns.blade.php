@if(isset($campaigns['error']))
    <p>{{ $campaigns['error'] }}</p>
@else
    <div class="container-fluid mt-2">
        <div class="table_wrap">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Fields</th>
                    @foreach ($campaigns as $index => $campaign)
                        <th scope="col">{{ $campaign['Name'] }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($selectedFields as $field)
                    <tr>
                        <td> {{ $field }}</td>
                        @foreach ($campaigns as $index => $campaign)
                            @if(isset($campaign[$field]) && gettype($campaign[$field]) == 'string')
                                <td>{{ $campaign[$field] }}</td>
                            @elseif($field == 'Statistics')
                                <td>
                                    <p>Клики: {{ $campaign[$field]->Clicks }}</p>
                                    <p>Показы: {{ $campaign[$field]->Impressions }}</p>
                                </td>
                            @elseif($field == 'Funds')
                                <td>
                                    @if($campaign['Funds']->Mode == 'CAMPAIGN_FUNDS')
                                        <p>Зачисленная сумма за все
                                            время: {{ round($campaign[$field]->CampaignFunds->Balance / 1000000, 2) }}</p>
                                        <p>Баланс кампании: {{ $campaign[$field]->CampaignFunds->Sum }}</p>
                                    @elseif($campaign['Funds']->Mode == 'SHARED_ACCOUNT_FUNDS')
                                        <p>Израсходованная сумма за все
                                            время: {{ round($campaign[$field]->SharedAccountFunds->Spend / 1000000, 2) }}</p>
                                    @endif
                                </td>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @if(isset($paging))
            <div class="btn-group">
                @if($paging['Offset'] > $paging['Limit'])
                    <form action="{{ url('yandex/get-campaigns') }}" method="POST" class="ajax__form">
                        {{ csrf_field() }}
                        <input type="hidden" id="pageOffset" name="pageOffset"
                               value="{{ $paging['Prev'] }}">
                        <input type="hidden" name="entriesPerPage" value="{{ $paging['Limit'] }}">
                        @foreach($paging['fields'] as $field)
                            <input type="hidden" name="fields[{{ $field }}]" value="{{ $field }}" checked="checked">
                        @endforeach
                        <button type="submit" class="btn btn-large waves-effect waves-light">Назад</button>
                    </form>
                @endif
                @if($paging['Offset'] >= $paging['Limit'] && $paging['Next'] != 'N')
                    <form action="{{ url('yandex/get-campaigns') }}" method="POST" class="ajax__form">
                        {{ csrf_field() }}
                        <input type="hidden" id="pageOffset" name="pageOffset"
                               value="{{ $paging['Offset'] }}">
                        <input type="hidden" name="entriesPerPage" value="{{ $paging['Limit'] }}">
                        @foreach($paging['fields'] as $field)
                            <input type="hidden" name="fields[{{ $field }}]" value="{{ $field }}" checked="checked">
                        @endforeach
                        <button type="submit" class="btn btn-large waves-effect waves-light">Вперед</button>
                    </form>
                @endif
            </div>
        @endif
    </div>
@endif
{{--<div class="container-fluid mt-2">--}}
{{--    <table class="table">--}}
{{--        <thead>--}}
{{--        <tr>--}}
{{--            <th scope="col">#</th>--}}
{{--            @foreach ($selectedFields as $field)--}}
{{--                <th scope="col">{{ $field }}</th>--}}
{{--            @endforeach--}}
{{--        </tr>--}}
{{--        </thead>--}}
{{--        <tbody>--}}
{{--        @foreach ($campaigns as $index => $campaign)--}}
{{--            <tr>--}}
{{--                <td>{{ $index }}</td>--}}
{{--                @foreach($selectedFields as $field)--}}
{{--                    @if(isset($campaign[$field]) && gettype($campaign[$field]) == 'string')--}}
{{--                        <td>{{ $campaign[$field] }}</td>--}}
{{--                    @elseif($field == 'Statistics')--}}
{{--                        <td>--}}
{{--                            <p>Clicks: {{ $campaign[$field]->Clicks }}</p>--}}
{{--                            <p>Impressions: {{ $campaign[$field]->Impressions }}</p>--}}
{{--                        </td>--}}
{{--                    @else--}}
{{--                        <td></td>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--            </tr>--}}
{{--        @endforeach--}}
{{--        </tbody>--}}
{{--    </table>--}}
{{--    @if(isset($paging))--}}
{{--        @if($paging['Offset'] > $paging['Limit'])--}}
{{--            <form action="{{ url('yandex/get-campaigns') }}" method="POST" class="ajax__form">--}}
{{--                {{ csrf_field() }}--}}
{{--                <input type="hidden" id="pageOffset" name="pageOffset"--}}
{{--                       value="{{ $paging['Prev'] }}">--}}
{{--                <input type="hidden" name="entriesPerPage" value="{{ $paging['Limit'] }}">--}}
{{--                @foreach($paging['fields'] as $field)--}}
{{--                    <input type="hidden" name="fields[{{ $field }}]" value="{{ $field }}" checked="checked">--}}
{{--                @endforeach--}}
{{--                <button type="submit" class="btn btn-primary">Prev</button>--}}
{{--            </form>--}}
{{--        @endif--}}
{{--        @if($paging['Offset'] >= $paging['Limit'] && $paging['Next'] != 'N')--}}
{{--            <form action="{{ url('yandex/get-campaigns') }}" method="POST" class="ajax__form">--}}
{{--                {{ csrf_field() }}--}}
{{--                <input type="hidden" id="pageOffset" name="pageOffset"--}}
{{--                       value="{{ $paging['Offset'] }}">--}}
{{--                <input type="hidden" name="entriesPerPage" value="{{ $paging['Limit'] }}">--}}
{{--                @foreach($paging['fields'] as $field)--}}
{{--                    <input type="hidden" name="fields[{{ $field }}]" value="{{ $field }}" checked="checked">--}}
{{--                @endforeach--}}
{{--                <button type="submit" class="btn btn-primary">Next</button>--}}
{{--            </form>--}}
{{--        @endif--}}
{{--    @endif--}}
{{--</div>--}}
