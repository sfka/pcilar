<div class="row">
    <div class="col-md-12">
        <h3>Yandex</h3>
    </div>
    <div class="col-md-4">
        <div class="block_title">Бюджет</div>
        <p>Общий расход: @if(isset($arResult['Cost'])){{ round($arResult['Cost'], 2) }}@else не подсчитан@endif</p>
        <p>Сумма остатка: @if(isset($arResult['Balance'])){{ $arResult['Balance'] }}@else не подсчитан@endif</p>
    </div>
    <div class="col-md-4">
        <div class="block_title">Статистика</div>
        <p>Стоимость заявки: @if(isset($arResult['OrderCost'])){{ $arResult['OrderCost'] }}@else не
            подсчитан@endif</p>
        <p>Стоимость клика: @if(isset($arResult['AvgCpc'])){{ $arResult['AvgCpc'] }}@else не подсчитан@endif</p>
        <p>Количество кликов: @if(isset($arResult['Clicks'])){{ $arResult['Clicks'] }}@else не подсчитан@endif</p>
        <p>Количество показов: @if(isset($arResult['Impressions'])){{ $arResult['Impressions'] }}@else не
            подсчитан@endif</p>
    </div>
    <div class="col-md-4">
        <div class="block_title">Лиды</div>
        <p>Количество лидов: {{ $arResult['metric']['totalGoalsVisits'] }}
            @if(isset($arResult['metric']['visits']))
                @foreach($arResult['goals'] as $arGoal)
                    <p>{{ $arGoal['name'] }}: {{ $arGoal['visits'] }}</p>
                @endforeach
            @else не подсчитан@endif</p>
        <p>Конверсия:
            @if(isset($arResult['metric']['visits']))
                {{ round($arResult['metric']['visits'] / $arResult['metric']['totalGoalsVisits'], 2)  }}
            @else не подсчитан@endif</p>
    </div>
</div>
