<div class="row">
    <div class="col-md-12">
        <h3>Facebook</h3>
    </div>
    @if(isset($arResult['error']))
        {{ $arResult['error'] }}
    @else
    <div class="col-md-4">
        <div class="block_title">Бюджет</div>
        <p>Расход: @if(isset($arResult['spend'])){{ $arResult['spend'] }}@else не подсчитан@endif</p>
        <p>Сумма остатка: @if(isset($arResult['balance'])){{ $arResult['balance'] }}@else не подсчитан@endif</p>
        <p>Прогноз бюджета: @if(isset($arResult['forecast'])){{ $arResult['forecast'] }}@else не подсчитан@endif</p>
    </div>
    <div class="col-md-4">
        <div class="block_title">Статистика</div>
        <p>Стоимость заявки: @if(isset($arResult['cost_per_conversion'])){{ $arResult['cost_per_conversion'] }}@else не подсчитан@endif</p>
        <p>Стоимость клика: @if(isset($arResult['cpc'])){{ $arResult['cpc'] }}@else не подсчитан@endif</p>
        <p>Количество кликов: @if(isset($arResult['clicks'])){{ $arResult['clicks'] }}@else не подсчитан@endif</p>
        <p>Количество показов: @if(isset($arResult['impressions'])){{ $arResult['impressions'] }}@else не подсчитан@endif</p>
    </div>
    <div class="col-md-4">
        <div class="block_title">Лиды</div>
        <p>Количество лидов: @if(isset($arResult['cost_per_conversion'])){{ $arResult['cost_per_conversion'] }}@else не подсчитан@endif</p>
        <p>Конверсия: @if(isset($arResult['conversions'])){{ $arResult['conversions'] }}@else не подсчитан@endif</p>
    </div>
    @endif
</div>
