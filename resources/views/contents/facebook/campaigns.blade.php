<div class="row">
    <div class="col s12">
        @if (isset($campaigns['data']))
            <div class="table_wrap">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Поля</th>
                        @foreach ($campaigns['data'] as $index => $campaign)
                            <th scope="col">{{ $campaign['name'] }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($selectedFields as $field)
                        <tr>
                            <td><span class="js__prop_description">{{ $field }}</span></td>
                            @foreach ($campaigns['data'] as $index => $campaign)
                                @if(isset($campaign[$field]) && gettype($campaign[$field]) != 'array' && gettype($campaign[$field]) != 'object')
                                    @if($field == 'name')
                                        <td>
                                            <a href="" class="js__campaign_input"
                                               data-id="{{ $campaign['id'] }}">{{ $campaign[$field] }}</a>
                                        </td>
                                    @elseif($field == 'id')
                                        <td></td>
                                    @else
                                        <td>{{ $campaign[$field] }}</td>
                                    @endif
                                @elseif(isset($campaign[$field]) && gettype($campaign[$field]) == 'array')
                                    @foreach($campaign[$field] as $campField)
                                        <td><p>{{ $campField }}</p></td>
                                    @endforeach
                                @else
                                    <td></td>
                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @elseif(isset($campaigns['error']))
            <p>{{ $campaigns['error'] }}</p>
        @endif
        @if(isset($campaigns['page']['previous']))
            <form action="{{ url('facebook/get-campaigns') }}" method="POST" class="ajax__form">
                {{ csrf_field() }}
                <input type="hidden" name="previous" value="{{ $campaigns['page']['previous'] }}">
                <button type="submit" class="btn btn-primary">Previous</button>
            </form>
        @endif
        @if(isset($campaigns['page']['next']))
            <form action="{{ url('facebook/get-campaigns') }}" method="POST" class="ajax__form">
                {{ csrf_field() }}
                <input type="hidden" name="next" value="{{ $campaigns['page']['next'] }}">
                <button type="submit" class="btn btn-primary">Next</button>
            </form>
        @endif
    </div>
</div>
