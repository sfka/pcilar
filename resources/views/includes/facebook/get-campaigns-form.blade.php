<form action="{{ url('facebook/get-campaigns') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col s12">
            <p>Получить список кампаний клиента.</p>
        </div>
    </div>
    <div class="row">
        <div class="col s12 input-field">
            Поля для извлечения:
        </div>
        <div class="col s12">
            <div class="checkbox-list">
                <label>
                    <input type="checkbox" class="filled-in all_checkbox" checked>
                    <span>Выбрать все</span>
                </label>
                @include('includes.form-row', ['field' => 'id', 'disabled' => 'Y'])
                @include('includes.form-row', ['field' => 'name', 'disabled' => 'Y'])
                @include('includes.form-row', ['field' => 'adlabels'])
                @include('includes.form-row', ['field' => 'bid_strategy'])
                @include('includes.form-row', ['field' => 'boosted_object_id'])
                @include('includes.form-row', ['field' => 'brand_lift_studies'])
                @include('includes.form-row', ['field' => 'budget_rebalance_flag'])
                @include('includes.form-row', ['field' => 'budget_remaining'])
                @include('includes.form-row', ['field' => 'buying_type'])
                @include('includes.form-row', ['field' => 'can_create_brand_lift_study'])
                @include('includes.form-row', ['field' => 'can_use_spend_cap'])
                @include('includes.form-row', ['field' => 'configured_status'])
                @include('includes.form-row', ['field' => 'created_time'])
                @include('includes.form-row', ['field' => 'daily_budget'])
                @include('includes.form-row', ['field' => 'effective_status'])
                @include('includes.form-row', ['field' => 'issues_info'])
                @include('includes.form-row', ['field' => 'last_budget_toggling_time'])
                @include('includes.form-row', ['field' => 'lifetime_budget'])
                @include('includes.form-row', ['field' => 'objective'])
                @include('includes.form-row', ['field' => 'pacing_type'])
                @include('includes.form-row', ['field' => 'promoted_object'])
                @include('includes.form-row', ['field' => 'recommendations'])
                @include('includes.form-row', ['field' => 'source_campaign'])
                @include('includes.form-row', ['field' => 'source_campaign_id'])
                @include('includes.form-row', ['field' => 'special_ad_categories'])
                @include('includes.form-row', ['field' => 'special_ad_category'])
                @include('includes.form-row', ['field' => 'special_ad_category_country'])
                @include('includes.form-row', ['field' => 'spend_cap'])
                @include('includes.form-row', ['field' => 'start_time'])
                @include('includes.form-row', ['field' => 'status'])
                @include('includes.form-row', ['field' => 'stop_time'])
                @include('includes.form-row', ['field' => 'topline_id'])
                @include('includes.form-row', ['field' => 'updated_time'])
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="effective_status" name="effective_status[]">
                <option selected>ACTIVE</option>
                <option>PAUSED</option>
                <option>DELETED</option>
                <option>PENDING_REVIEW</option>
                <option>DISAPPROVED</option>
                <option>PREAPPROVED</option>
                <option>PENDING_BILLING_INFO</option>
                <option>CAMPAIGN_PAUSED</option>
                <option>ARCHIVED</option>
                <option>ADSET_PAUSED</option>
                <option>IN_PROCESS</option>
                <option>WITH_ISSUES</option>
            </select>
            <label>effective_status:</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <input id="dateFrom" class="datepicker" type="text" name="dateFrom">
            <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
        </div>
        <div class="col m6 s12 input-field">
            <input id="dateTo" class="datepicker" type="text" name="dateTo">
            <label for="dateTo">Дата конца: dd / mm / yyyy</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="entriesPerPage" name="entriesPerPage">
                <option selected>20</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
            </select>
            <label>Количество строк на странице</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Получить все кампании
            </button>
        </div>
    </div>
</form>
