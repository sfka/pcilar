<form id="campaign_input" action="{{ url('facebook/campaign-info') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <input id="campaignID" type="hidden" name="campaignID" value="">
    <div class="row">
        <div class="col s12">
            <p>Получить детальную информацию о кампании</p>
            <small>Для того чтобы получить детальную информацию о кампании, необходимо получить список кампаний, в появившемся списке кликнуть на необходимую кампанию и выбрать поля ниже</small>
        </div>
    </div>
    <div class="row">
        <div class="col s12 input-field">
            Поля для извлечения:
        </div>
        <div class="col s12">
            <div class="checkbox-list">
                <label>
                    <input type="checkbox" class="filled-in">
                    <span>all</span>
                </label>
                @include('includes.form-row', ['field' => 'campaign_id'])
                @include('includes.form-row', ['field' => 'campaign_name'])
                @include('includes.form-row', ['field' => 'account_currency'])
                @include('includes.form-row', ['field' => 'account_id'])
                @include('includes.form-row', ['field' => 'account_name'])
                @include('includes.form-row', ['field' => 'action_values'])
                @include('includes.form-row', ['field' => 'actions'])
                @include('includes.form-row', ['field' => 'ad_id'])
                @include('includes.form-row', ['field' => 'ad_name'])
                @include('includes.form-row', ['field' => 'adset_id'])
                @include('includes.form-row', ['field' => 'adset_name'])
                @include('includes.form-row', ['field' => 'buying_type'])
                @include('includes.form-row', ['field' => 'canvas_avg_view_percent'])
                @include('includes.form-row', ['field' => 'canvas_avg_view_time'])
                @include('includes.form-row', ['field' => 'catalog_segment_value'])
                @include('includes.form-row', ['field' => 'clicks'])
                @include('includes.form-row', ['field' => 'conversion_values'])
                @include('includes.form-row', ['field' => 'conversions'])
                @include('includes.form-row', ['field' => 'converted_product_quantity'])
                @include('includes.form-row', ['field' => 'converted_product_value'])
                @include('includes.form-row', ['field' => 'cost_per_action_type'])
                @include('includes.form-row', ['field' => 'cost_per_conversion'])
                @include('includes.form-row', ['field' => 'cost_per_estimated_ad_recallers'])
                @include('includes.form-row', ['field' => 'cost_per_inline_link_click'])
                @include('includes.form-row', ['field' => 'cost_per_inline_post_engagement'])
                @include('includes.form-row', ['field' => 'cost_per_outbound_click'])
                @include('includes.form-row', ['field' => 'cost_per_thruplay'])
                @include('includes.form-row', ['field' => 'cost_per_unique_action_type'])
                @include('includes.form-row', ['field' => 'cost_per_unique_click'])
                @include('includes.form-row', ['field' => 'cost_per_unique_inline_link_click'])
                @include('includes.form-row', ['field' => 'cost_per_unique_outbound_click'])
                @include('includes.form-row', ['field' => 'cpc'])
                @include('includes.form-row', ['field' => 'cpm'])
                @include('includes.form-row', ['field' => 'cpp'])
                @include('includes.form-row', ['field' => 'ctr'])
                @include('includes.form-row', ['field' => 'date_start'])
                @include('includes.form-row', ['field' => 'date_stop'])
                @include('includes.form-row', ['field' => 'estimated_ad_recall_rate'])
                @include('includes.form-row', ['field' => 'estimated_ad_recallers'])
                @include('includes.form-row', ['field' => 'frequency'])
                @include('includes.form-row', ['field' => 'full_view_impressions'])
                @include('includes.form-row', ['field' => 'full_view_reach'])
                @include('includes.form-row', ['field' => 'impressions'])
                @include('includes.form-row', ['field' => 'inline_link_click_ctr'])
                @include('includes.form-row', ['field' => 'inline_link_clicks'])
                @include('includes.form-row', ['field' => 'inline_post_engagement'])
                @include('includes.form-row', ['field' => 'instant_experience_clicks_to_open'])
                @include('includes.form-row', ['field' => 'instant_experience_clicks_to_start'])
                @include('includes.form-row', ['field' => 'instant_experience_outbound_clicks'])
                @include('includes.form-row', ['field' => 'mobile_app_purchase_roas'])
                @include('includes.form-row', ['field' => 'objective'])
                @include('includes.form-row', ['field' => 'outbound_clicks'])
                @include('includes.form-row', ['field' => 'outbound_clicks_ctr'])
                @include('includes.form-row', ['field' => 'place_page_name'])
                @include('includes.form-row', ['field' => 'purchase_roas'])
                @include('includes.form-row', ['field' => 'qualifying_question_qualify_answer_rate'])
                @include('includes.form-row', ['field' => 'reach'])
                @include('includes.form-row', ['field' => 'social_spend'])
                @include('includes.form-row', ['field' => 'spend'])
                @include('includes.form-row', ['field' => 'unique_actions'])
                @include('includes.form-row', ['field' => 'unique_clicks'])
                @include('includes.form-row', ['field' => 'unique_ctr'])
                @include('includes.form-row', ['field' => 'unique_inline_link_click_ctr'])
                @include('includes.form-row', ['field' => 'unique_inline_link_clicks'])
                @include('includes.form-row', ['field' => 'unique_link_clicks_ctr'])
                @include('includes.form-row', ['field' => 'unique_outbound_clicks'])
                @include('includes.form-row', ['field' => 'unique_outbound_clicks_ctr'])
                @include('includes.form-row', ['field' => 'video_30_sec_watched_actions'])
                @include('includes.form-row', ['field' => 'video_avg_time_watched_actions'])
                @include('includes.form-row', ['field' => 'video_p100_watched_actions'])
                @include('includes.form-row', ['field' => 'video_p25_watched_actions'])
                @include('includes.form-row', ['field' => 'video_p50_watched_actions'])
                @include('includes.form-row', ['field' => 'video_p75_watched_actions'])
                @include('includes.form-row', ['field' => 'video_p95_watched_actions'])
                @include('includes.form-row', ['field' => 'video_play_actions'])
                @include('includes.form-row', ['field' => 'video_play_curve_actions'])
                @include('includes.form-row', ['field' => 'website_ctr'])
                @include('includes.form-row', ['field' => 'website_purchase_roas'])
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <input id="dateFrom2" class="datepicker" type="text" name="dateFrom">
            <label for="dateFrom2">Дата начала: dd / mm / yyyy</label>
        </div>
        <div class="col m6 s12 input-field">
            <input id="dateTo2" class="datepicker" type="text" name="dateTo">
            <label for="dateTo2">Дата конца: dd / mm / yyyy</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Получить информацию о кампании
            </button>
        </div>
    </div>
</form>
