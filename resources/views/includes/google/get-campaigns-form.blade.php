<form action="{{ url('google/get-campaigns') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col s12">
            <p>Получить список кампаний клиента.</p>
        </div>
    </div>
    <div class="row">
        <div class="col s12 input-field">
            Поля для извлечения:
        </div>
        <div class="col s12">
            <div class="checkbox-list">
                <label>
                    <input type="checkbox" class="filled-in all_checkbox" checked>
                    <span>Выбрать все</span>
                </label>
                @include('includes.form-row', ['field' => 'Id', 'disabled' => 'Y'])
                @include('includes.form-row', ['field' => 'Name'])
                @include('includes.form-row', ['field' => 'Status'])
                @include('includes.form-row', ['field' => 'ServingStatus'])
                @include('includes.form-row', ['field' => 'AdvertisingChannelType'])
                @include('includes.form-row', ['field' => 'AdvertisingChannelSubType'])
                @include('includes.form-row', ['field' => 'AllConversions'])
                @include('includes.form-row', ['field' => 'AbsoluteTopImpressionPercentage'])
                @include('includes.form-row', ['field' => 'AccountCurrencyCode'])
                @include('includes.form-row', ['field' => 'AccountDescriptiveName'])
                @include('includes.form-row', ['field' => 'AccountTimeZone'])
                @include('includes.form-row', ['field' => 'ActiveViewCpm'])
                @include('includes.form-row', ['field' => 'ActiveViewCtr'])
                @include('includes.form-row', ['field' => 'ActiveViewImpressions'])
                @include('includes.form-row', ['field' => 'ActiveViewMeasurability'])
                @include('includes.form-row', ['field' => 'ActiveViewMeasurableCost'])
                @include('includes.form-row', ['field' => 'ActiveViewMeasurableImpressions'])
                @include('includes.form-row', ['field' => 'ActiveViewViewability'])
                @include('includes.form-row', ['field' => 'AdNetworkType1'])
                @include('includes.form-row', ['field' => 'AdNetworkType2'])
                @include('includes.form-row', ['field' => 'AdvertisingChannelSubType'])
                @include('includes.form-row', ['field' => 'AdvertisingChannelType'])
                @include('includes.form-row', ['field' => 'AllConversions'])
                @include('includes.form-row', ['field' => 'Amount'])
                @include('includes.form-row', ['field' => 'AverageCost'])
                @include('includes.form-row', ['field' => 'AverageCpe'])
                @include('includes.form-row', ['field' => 'AverageCpv'])
                @include('includes.form-row', ['field' => 'AveragePageviews'])
                @include('includes.form-row', ['field' => 'AverageTimeOnSite'])
                @include('includes.form-row', ['field' => 'BaseCampaignId'])
                @include('includes.form-row', ['field' => 'BiddingStrategyId'])
                @include('includes.form-row', ['field' => 'BiddingStrategyName'])
                @include('includes.form-row', ['field' => 'BiddingStrategyType'])
                @include('includes.form-row', ['field' => 'BounceRate'])
                @include('includes.form-row', ['field' => 'BudgetId'])
                @include('includes.form-row', ['field' => 'CampaignDesktopBidModifier'])
                @include('includes.form-row', ['field' => 'CampaignGroupId'])
                @include('includes.form-row', ['field' => 'CampaignId'])
                @include('includes.form-row', ['field' => 'CampaignMobileBidModifier'])
                @include('includes.form-row', ['field' => 'CampaignName'])
                @include('includes.form-row', ['field' => 'CampaignStatus'])
                @include('includes.form-row', ['field' => 'CampaignTabletBidModifier'])
                @include('includes.form-row', ['field' => 'CampaignTrialType'])
                @include('includes.form-row', ['field' => 'ClickAssistedConversions'])
                @include('includes.form-row', ['field' => 'ClickAssistedConversionsOverLastClickConversions'])
                @include('includes.form-row', ['field' => 'ClickAssistedConversionValue'])
                @include('includes.form-row', ['field' => 'ContentBudgetLostImpressionShare'])
                @include('includes.form-row', ['field' => 'ContentImpressionShare'])
                @include('includes.form-row', ['field' => 'ContentRankLostImpressionShare'])
                @include('includes.form-row', ['field' => 'ConversionRate'])
                @include('includes.form-row', ['field' => 'ConversionValue'])
                @include('includes.form-row', ['field' => 'CostPerAllConversion'])
                @include('includes.form-row', ['field' => 'CostPerConversion'])
                @include('includes.form-row', ['field' => 'CostPerCurrentModelAttributedConversion'])
                @include('includes.form-row', ['field' => 'CrossDeviceConversions'])
                @include('includes.form-row', ['field' => 'CurrentModelAttributedConversions'])
                @include('includes.form-row', ['field' => 'CurrentModelAttributedConversionValue'])
                @include('includes.form-row', ['field' => 'CustomerDescriptiveName'])
                @include('includes.form-row', ['field' => 'Date'])
                @include('includes.form-row', ['field' => 'EndDate'])
                @include('includes.form-row', ['field' => 'EngagementRate'])
                @include('includes.form-row', ['field' => 'Engagements'])
                @include('includes.form-row', ['field' => 'EnhancedCpcEnabled'])
                @include('includes.form-row', ['field' => 'ExternalCustomerId'])
                @include('includes.form-row', ['field' => 'FinalUrlSuffix'])
                @include('includes.form-row', ['field' => 'GmailForwards'])
                @include('includes.form-row', ['field' => 'GmailSaves'])
                @include('includes.form-row', ['field' => 'GmailSecondaryClicks'])
                @include('includes.form-row', ['field' => 'HasRecommendedBudget'])
                @include('includes.form-row', ['field' => 'ImpressionAssistedConversions'])
                @include('includes.form-row', ['field' => 'ImpressionAssistedConversionsOverLastClickConversions'])
                @include('includes.form-row', ['field' => 'ImpressionAssistedConversionValue'])
                @include('includes.form-row', ['field' => 'ImpressionReach'])
                @include('includes.form-row', ['field' => 'InteractionRate'])
                @include('includes.form-row', ['field' => 'Interactions'])
                @include('includes.form-row', ['field' => 'InteractionTypes'])
                @include('includes.form-row', ['field' => 'IsBudgetExplicitlyShared'])
                @include('includes.form-row', ['field' => 'LabelIds'])
                @include('includes.form-row', ['field' => 'Labels'])
                @include('includes.form-row', ['field' => 'MaximizeConversionValueTargetRoas'])
                @include('includes.form-row', ['field' => 'Month'])
                @include('includes.form-row', ['field' => 'MonthOfYear'])
                @include('includes.form-row', ['field' => 'PercentNewVisitors'])
                @include('includes.form-row', ['field' => 'RecommendedBudgetAmount'])
                @include('includes.form-row', ['field' => 'RelativeCtr'])
                @include('includes.form-row', ['field' => 'SearchAbsoluteTopImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchBudgetLostAbsoluteTopImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchBudgetLostImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchBudgetLostTopImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchClickShare'])
                @include('includes.form-row', ['field' => 'SearchExactMatchImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchRankLostAbsoluteTopImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchRankLostImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchRankLostTopImpressionShare'])
                @include('includes.form-row', ['field' => 'SearchTopImpressionShare'])
                @include('includes.form-row', ['field' => 'ServingStatus'])
                @include('includes.form-row', ['field' => 'StartDate'])
                @include('includes.form-row', ['field' => 'TopImpressionPercentage'])
                @include('includes.form-row', ['field' => 'TotalAmount'])
                @include('includes.form-row', ['field' => 'TrackingUrlTemplate'])
                @include('includes.form-row', ['field' => 'UrlCustomParameters'])
                @include('includes.form-row', ['field' => 'ValuePerAllConversion'])
                @include('includes.form-row', ['field' => 'ValuePerConversion'])
                @include('includes.form-row', ['field' => 'ValuePerCurrentModelAttributedConversion'])
                @include('includes.form-row', ['field' => 'VideoQuartile100Rate'])
                @include('includes.form-row', ['field' => 'VideoQuartile25Rate'])
                @include('includes.form-row', ['field' => 'VideoQuartile50Rate'])
                @include('includes.form-row', ['field' => 'VideoQuartile75Rate'])
                @include('includes.form-row', ['field' => 'VideoViewRate'])
                @include('includes.form-row', ['field' => 'VideoViews'])
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="entriesPerPage" name="entriesPerPage">
                <option selected>20</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
            </select>
            <label>Количество строк на странице</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Получить все кампании
            </button>
        </div>
    </div>
</form>
