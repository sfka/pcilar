<form action="{{ url('google/download-report') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col s12">
            <p>
                Получить отчет для клиента
            </p>
            <p>
                Все отчеты загружаются <em>без</em>
                <a href="https://developers.google.com/adwords/api/docs/guides/zeroimpression-structure-reports">строк с нулевым показом</a>
                для краткости.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="reportType" name="reportType">
                <option selected>CAMPAIGN_PERFORMANCE_REPORT</option>
                <option>ADGROUP_PERFORMANCE_REPORT</option>
                <option>AD_PERFORMANCE_REPORT</option>
                <option>ACCOUNT_PERFORMANCE_REPORT</option>
                <option>BUDGET_PERFORMANCE_REPORT</option>
            </select>
            <label>
                Тип отчета
                <small id="reportTypeHelp">
                    Просмотреть все типы отчетов
                    <a href="https://developers.google.com/adwords/api/docs/appendix/reports/all-reports" target="_blank">здесь</a>.
                </small>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <div class="checkbox-list">
                <label>
                    <input type="checkbox" class="filled-in all_checkbox" checked>
                    <span>Выбрать все</span>
                </label>
                <label>
                    <input id="impressions" class="filled-in" type="checkbox" name="impressions" value="Impressions" checked>
                    <span><code>Impressions</code></span>
                </label>
                <label>
                    <input id="clicks" class="filled-in" type="checkbox" name="clicks" value="Clicks" checked>
                    <span><code>Clicks</code></span>
                </label>
                <label>
                    <input id="ctr" class="filled-in" type="checkbox" name="ctr" value="Ctr" checked>
                    <span><code>Ctr</code></span>
                </label>
                <label>
                    <input id="cost" class="filled-in" type="checkbox" name="cost" value="Cost" checked>
                    <span><code>Cost</code></span>
                </label>
                <label>
                    <input id="conversions" class="filled-in" type="checkbox" name="conversions" value="Conversions" checked>
                    <span><code>Conversions</code></span>
                </label>
                <label>
                    <input id="crossDeviceConversions" class="filled-in" type="checkbox" name="crossDeviceConversions" value="CrossDeviceConversions" checked>
                    <span><code>CrossDeviceConversions</code></span>
                </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <input id="dateFrom" class="datepicker" type="text" name="dateFrom">
            <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
        </div>
        <div class="col m6 s12 input-field">
            <input id="dateTo" class="datepicker" type="text" name="dateTo">
            <label for="dateTo">Дата конца: dd / mm / yyyy</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="entriesPerPage" name="entriesPerPage">
                <option selected>20</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
            </select>
            <label>Количество строк на странице</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Скачать отчет
            </button>
        </div>
    </div>
</form>

