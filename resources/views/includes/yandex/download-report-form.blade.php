<form action="{{ url('yandex/report-results') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col s12">
            <p>
                Скачать отчет для клиента
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="reportType" name="reportType">
                <option selected>CAMPAIGN_PERFORMANCE_REPORT</option>
                <option>ADGROUP_PERFORMANCE_REPORT</option>
                <option>AD_PERFORMANCE_REPORT</option>
                <option>ACCOUNT_PERFORMANCE_REPORT</option>
            </select>
            <label>
                Тип отчета
                <small id="reportTypeHelp">
                    Просмотреть все типы отчетов
                    <a href="https://yandex.ru/dev/direct/doc/reports/type-docpage/" target="_blank">здесь</a>.
                </small>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <div class="checkbox-list">
                <label>
                    <input type="checkbox" class="filled-in all_checkbox" checked>
                    <span>Выбрать все</span>
                </label>
                @include('includes.form-row', ['field' => 'AdFormat', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AdGroupName', 'type' => ['ADGROUP_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AdId', 'type' => ['AD_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AdNetworkType', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Age', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AvgClickPosition', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AvgCpc', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AvgImpressionPosition', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AvgPageviews', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'AvgTrafficVolume', 'type' => ['ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'BounceRate', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Bounces', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'CampaignName', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'CampaignType', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'CarrierType', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Clicks', 'type' => ['ACCOUNT_PERFORMANCE_REPORT', 'CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'ConversionRate', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Conversions', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Cost', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'CostPerConversion', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Ctr', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Date', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Device', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'DynamicTextAdTargetId'])
                @include('includes.form-row', ['field' => 'ExternalNetworkName', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Gender', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'GoalsRoi', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Impressions', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'LocationOfPresenceName', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'MatchType', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'MobilePlatform', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Placement', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Profit', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT'], 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT'])
                @include('includes.form-row', ['field' => 'Revenue', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT', 'AD_PERFORMANCE_REPORT', 'ACCOUNT_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Sessions', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'Slot', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'TargetingLocationId', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT']])
                @include('includes.form-row', ['field' => 'TargetingLocationName', 'type' => ['CAMPAIGN_PERFORMANCE_REPORT', 'ADGROUP_PERFORMANCE_REPORT']])
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <input id="dateFrom" class="datepicker" type="text" name="dateFrom">
            <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
        </div>
        <div class="col m6 s12 input-field">
            <input id="dateTo" class="datepicker" type="text" name="dateTo">
            <label for="dateTo">Дата конца: dd / mm / yyyy</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Скачать отчет
            </button>
        </div>
    </div>
</form>

