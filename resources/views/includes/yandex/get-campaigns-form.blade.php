<form action="{{ url('yandex/get-campaigns') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col s12">
            <p>Получить список кампаний клиента.</p>
        </div>
    </div>
    <div class="row">
        <div class="col s12 input-field">
            Поля для извлечения:
        </div>
        <div class="col s12">
            <div class="checkbox-list">
                <label>
                    <input type="checkbox" class="filled-in all_checkbox" checked>
                    <span>Выбрать все</span>
                </label>
                @include('includes.form-row', ['field' => 'Id', 'disabled' => 'Y'])
                @include('includes.form-row', ['field' => 'Name', 'disabled' => 'Y'])
                @include('includes.form-row', ['field' => 'BlockedIps'])
                @include('includes.form-row', ['field' => 'ClientInfo'])
                @include('includes.form-row', ['field' => 'Currency'])
                @include('includes.form-row', ['field' => 'DailyBudget'])
                @include('includes.form-row', ['field' => 'EndDate'])
                @include('includes.form-row', ['field' => 'ExcludedSites'])
                @include('includes.form-row', ['field' => 'Funds'])
                @include('includes.form-row', ['field' => 'NegativeKeywords'])
                @include('includes.form-row', ['field' => 'Notification'])
                @include('includes.form-row', ['field' => 'RepresentedBy'])
                @include('includes.form-row', ['field' => 'SourceId'])
                @include('includes.form-row', ['field' => 'StartDate'])
                @include('includes.form-row', ['field' => 'State'])
                @include('includes.form-row', ['field' => 'Statistics'])
                @include('includes.form-row', ['field' => 'Status'])
                @include('includes.form-row', ['field' => 'StatusClarification'])
                @include('includes.form-row', ['field' => 'StatusPayment'])
                @include('includes.form-row', ['field' => 'TimeTargeting'])
                @include('includes.form-row', ['field' => 'TimeZone'])
                @include('includes.form-row', ['field' => 'Type'])
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="entriesPerPage" name="entriesPerPage">
                <option selected>20</option>
                <option>50</option>
                <option>100</option>
                <option>200</option>
            </select>
            <label>Количество строк на странице</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Получить все кампании
            </button>
        </div>
    </div>
</form>
