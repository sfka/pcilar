<label class="tooltipped js__label @if(isset($type)) @foreach($type as $tp) {{ $tp }} @endforeach @endif" data-position="bottom" data-tooltip="" data-field="{{ strtolower($field) }}">
    <input id="{{ $field }}" class="filled-in" type="checkbox" name="fields[{{ $field }}]" value="{{ $field }}"
           checked @if( isset($disabled) && $disabled == 'Y') disabled @endif>
    <span>
        <code>{{ $field }}</code>
    </span>
</label>
