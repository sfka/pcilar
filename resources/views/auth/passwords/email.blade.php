@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form class="col m6 s12 offset-s3" method="POST" action="<?php echo e(route('password.email')); ?>">
            <?php echo csrf_field(); ?>
            <div class="row">
                <div class="col s12">
                    <h4><?php echo e(__('Восстановление пароля')); ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <?php if(session('status')): ?>
                    <div class="alert alert-success" role="alert">
                        <?php echo e(session('status')); ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="email"><?php echo e(__('E-Mail Address')); ?></label>
                    <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
                    if (isset($message)) { $messageCache = $message; }
                    $message = $errors->first('email'); ?> is-invalid <?php unset($message);
                    if (isset($messageCache)) { $message = $messageCache; }
                    endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" autofocus>

                    <?php if ($errors->has('email')) :
                    if (isset($message)) { $messageCache = $message; }
                    $message = $errors->first('email'); ?>
                    <span class="invalid-feedback" role="alert">
                            <strong><?php echo e($message); ?></strong>
                        </span>
                    <?php unset($message);
                    if (isset($messageCache)) { $message = $messageCache; }
                    endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <button class="waves-effect waves-light btn btn-large" type="submit">
                        <?php echo e(__('Отправить')); ?>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
