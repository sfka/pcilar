@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form class="col m6 s12 offset-s3" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="row">
                <div class="col s12">
                    <h4>{{ __('Регистрация') }}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="name">{{ __('Имя') }}</label>
                    <input id="name" type="text" class="validate @error('name') invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="email">{{ __('E-mail') }}</label>
                    <input id="email" type="email" class="validate @error('email') invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="password">{{ __('Пароль') }}</label>
                    <input id="password" type="password" class="validate @error('password') invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="password-confirm">{{ __('Пароль ещё раз') }}</label>
                    <input id="password-confirm" type="password" class="validate" name="password_confirmation" required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <button class="waves-effect waves-light btn btn-large" type="submit">
                        {{ __('Регистрация') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
