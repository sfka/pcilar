@extends('layouts.app')
@section('content')
    <div class="dashboard" id="dashboard">
        <div class="row">
            <div class="col s12">
                <h3 class="dashboard__heading">Отчет по аккаунту за период {{ $arResult['dateRange']['DateFrom'] }}
                    - {{ $arResult['dateRange']['DateTo'] }}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <form action="/" method="POST" class="ajax__form">
                    {{ csrf_field() }}
                    @if(isset($arResult['USERS']))
                        <div class="row">
                            <div class="col m6 s12 input-field">
                                <select name="user_name">
                                    @foreach($arResult['USERS'] as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                <label>Идентификатор клиента</label>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col m6 s12 input-field">
                            <select name="platform">
                                @if(isset(Auth::user()->yandexLogin))
                                    <option>Yandex</option>@endif
                                @if(isset(Auth::user()->googleLogin))
                                    <option>Google</option>@endif
                                @if(isset(Auth::user()->facebookLogin))
                                    <option>Facebook</option>@endif
                                @if(isset(Auth::user()->callTrackLogin))
                                    <option>CallTrack</option>@endif
                            </select>
                            <label>Социальные сети:</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6 s12 input-field">
                            <input id="dateFrom1" class="datepicker" type="text" name="dateFrom1">
                            <label for="dateFrom1">Дата начала: dd / mm / yyyy</label>
                        </div>
                        <div class="col m6 s12 input-field">
                            <input id="dateTo1" class="datepicker" type="text" name="dateTo1">
                            <label for="dateTo1">Дата конца: dd / mm / yyyy</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6 s12 input-field">
                            <button class="btn waves-effect waves-light" type="submit">
                                Получить информацию
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="dynamic-content content_form">
            @if(isset($arResult['Info']['FACEBOOK_INFO']['1']))
                <?$fb = $arResult['Info']['FACEBOOK_INFO']['1']?>
                <div class="row">
                    <div class="col s12">
                        @if(isset($fb['error']))
                            <p>{{ $fb['error'] }}</p>
                        @else
                            <div class="dynamic-content__inner">
                                <h4>Facebook</h4>
                                <ul class="collection with-header">
                                    <li class="collection-header">
                                        <b>Бюджет</b>
                                    </li>
                                    @if(isset($fb['spend']))
                                        <li class="collection-item">
                                            Расход: {{ number_format($fb['spend'], 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($fb['balance']))
                                        <li class="collection-item">
                                            Сумма остатка: {{ number_format($fb['balance'], 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($fb['forecast']))
                                        <li class="collection-item">
                                            Прогноз бюджета (дней): {{ $fb['forecast'] }}
                                        </li>
                                    @endif
                                    <li class="collection-header">
                                        <b>Статистика</b>
                                    </li>
                                    @if(isset($fb['cpc']))
                                        <li class="collection-item">
                                            Стоимость клика: {{ number_format(round($fb['cpc'], 2), 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($fb['clicks']))
                                        <li class="collection-item">
                                            Количество кликов: {{ $fb['clicks'] }}
                                        </li>
                                    @endif
                                    @if(isset($fb['impressions']))
                                        <li class="collection-item">
                                            Количество показов: {{ $fb['impressions'] }}
                                        </li>
                                    @endif
                                    @if(isset($fb['Ctr']))
                                        <li class="collection-item">
                                            Ctr: {{ $fb['Ctr'] }} %
                                        </li>
                                    @endif
                                    @if(isset($fb['SORT_ACTIONS']))
                                        <li class="collection-header">
                                            <b>Лиды</b>
                                        </li>
                                        @foreach($fb['SORT_ACTIONS'] as $key => $action)
                                            <li class="collection-item">{{ $key }} : {{ $action['COUNT'] }}
                                                @if(isset($action['COST']))
                                                    <b>Цена:</b> {{ number_format(round($action['COST'], 2), 2, '.', ' ') }} ₽
                                                @endif
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            @elseif(isset($arResult['Info']['YANDEX_INFO']['1']))
                <? $ya = $arResult['Info']['YANDEX_INFO']['1']; ?>
                <div class="row">
                    <div class="col s12">
                        @if(isset($ya['error']))
                            <p>{{ $ya['error'] }}</p>
                        @else
                            <div class="dynamic-content__inner">
                                <h4>Yandex</h4>
                                <ul class="collection with-header">
                                    <li class="collection-header">
                                        <b>Бюджет</b>
                                    </li>
                                    @if(isset($ya['Cost']))
                                        <li class="collection-item">
                                            Общий расход: {{ number_format(round($ya['Cost'], 2), 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($ya['Balance']))
                                        <li class="collection-item">
                                            Сумма остатка: {{ number_format($ya['Balance'], 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($ya['forecast']))
                                        <li class="collection-item">
                                            Прогноз бюджета (дней): {{ $ya['forecast'] }}
                                        </li>
                                    @endif
                                    <li class="collection-header">
                                        <b>Статистика</b>
                                    </li>
                                    @if(isset($ya['OrderCost']))
                                        <li class="collection-item">
                                            Стоимость заявки: {{ number_format($ya['OrderCost'], 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($ya['AvgCpc']))
                                        <li class="collection-item">
                                            Стоимость клика: {{ number_format($ya['AvgCpc'], 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($ya['Clicks']))
                                        <li class="collection-item">
                                            Количество кликов: {{ $ya['Clicks'] }}
                                        </li>
                                    @endif
                                    @if(isset($ya['Impressions']))
                                        <li class="collection-item">
                                            Количество показов: {{ $ya['Impressions'] }}
                                        </li>
                                    @endif
                                    @if(isset($ya['Ctr']))
                                        <li class="collection-item">
                                            Ctr: {{ $ya['Ctr'] }} %
                                        </li>
                                    @endif
                                    @if(isset($ya['metric']['totalGoalsVisits']) && $ya['metric']['totalGoalsVisits'] > 0)
                                        <li class="collection-header">
                                            <b>Лиды</b>
                                        </li>
                                        <li class="collection-item">
                                            Количество лидов: {{ $ya['metric']['totalGoalsVisits'] }}
                                        </li>
                                        @if(isset($ya['metric']['visits']))
                                            @foreach($ya['goals'] as $arGoal)
                                                <li class="collection-item">
                                                    {{ $arGoal['name'] }}: {{ $arGoal['visits'] }}
                                                </li>
                                            @endforeach
                                        @endif
                                        @if(isset($ya['metric']['visits']))
                                            <li class="collection-item">
                                                Конверсия: {{ round($ya['metric']['visits'] / $ya['metric']['totalGoalsVisits'], 1)  }}
                                                %
                                            </li>
                                        @endif
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            @elseif(isset($arResult['Info']['GOOGLE_INFO']['1']))
                <? $go = $arResult['Info']['GOOGLE_INFO']['1']; ?>
                <div class="row">
                    <div class="col s12">
                        @if(isset($go['error']))
                            <p>{{ $go['error'] }}</p>
                        @else
                            <div class="dynamic-content__inner">
                                <h4>Google</h4>
                                <ul class="collection with-header">
                                    <li class="collection-header">
                                        <b>Бюджет</b>
                                    </li>
                                    @if(isset($go['cost']))
                                        <li class="collection-item">
                                            Общий расход: {{ number_format(round($go['cost'] / 1000000, 2), 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($go['balance']))
                                        <li class="collection-item">
                                            Примерный остаток: {{ number_format(round($go['balance'], 2), 2, '.', ' ') }} ₽
                                        </li>
                                    @endif
                                    @if(isset($go['forecast']))
                                        <li class="collection-item">
                                            Прогноз бюджета (дней): {{ $go['forecast'] }}
                                        </li>
                                    @endif
                                    <li class="collection-header">
                                        <b>Статистика</b>
                                    </li>
                                    @if(isset($go['avgCPC']))
                                        <li class="collection-item">
                                            Стоимость клика: {{ number_format(round($go['avgCPC'] / 1000000, 2), 2, '.', ' ') }}
                                        </li>
                                    @endif
                                    @if(isset($go['clicks']))
                                        <li class="collection-item">
                                            Количество кликов: {{ $go['clicks'] }}
                                        </li>
                                    @endif
                                    @if(isset($go['impressions']))
                                        <li class="collection-item">
                                            Количество показов: {{ $go['impressions'] }}
                                        </li>
                                    @endif
                                    @if(isset($go['Ctr']))
                                        <li class="collection-item">
                                            Ctr: {{ $go['Ctr'] }} %
                                        </li>
                                    @endif
                                    @if(isset($go['conversions']) && $go['conversions'] > 0)
                                        <li class="collection-header">
                                            <b>Лиды</b>
                                        </li>
                                        <li class="collection-item">
                                            Конверсия: {{ round($go['conversions']) }}
                                        </li>
                                        @if(isset($go['costConv']))
                                            <li class="collection-item">
                                                Стоимость заявки: {{ number_format(round($go['costConv'] / 1000000, 2), 2, '.', ' ') }} ₽
                                            </li>
                                        @endif
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>


            @elseif(isset($arResult['Info']['CALLTRACK_INFO']['1']))
                <? $ct = $arResult['Info']['CALLTRACK_INFO']['1']; ?>
                <div class="col s12">
                    @if(isset($ct['error']))
                        <p>{{ $ct['error'] }}</p>
                    @else
                        <div class="dynamic-content__inner">
                            <h4>Calltrack</h4>
                            <?$itogCt = 0; ?>
                            <table>
                                <thead>
                                <tr>
                                    @foreach($ct as $keyCt => $ctField)
                                        <th class="tac">{{ date('d.m.Y', strtotime($keyCt)) }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    @foreach($ct as $keyCt => $ctField)
                                        <td style="vertical-align: baseline">
                                            @foreach($ctField as $key => $field)
                                                @if($key != 'TOTAL_A' && $key != 'TOTAL_N' && $key != 'TOTAL_B' && $key != 'TOTAL_F')
                                                    <table>
                                                        <tr>
                                                            <td>{{ $key }}</td>
                                                            <td style="min-width: 150px">
                                                                @if(isset($field['ANSWERED']))
                                                                    отвеченных: {{ $field['ANSWERED']['ct:calls'] }}
                                                                @endif
                                                                @if(isset($field['NO ANSWER']))
                                                                    неотвеченных: {{ $field['NO ANSWER']['ct:calls'] }}
                                                                @endif
                                                                @if(isset($field['BUSY']))>
                                                                занято во время звонка: {{ $field['BUSY']['ct:calls'] }}
                                                                @endif
                                                                @if(isset($field['FAILED ']))
                                                                    ошибка
                                                                    оборудования: {{ $field['FAILED']['ct:calls'] }}
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </table>
                                                @endif
                                            @endforeach
                                        </td>
                                    @endforeach
                                </tr>
                                <tr>
                                    @foreach($ct as $keyCt => $ctField)
                                        <td>
                                            @if(($ctField['TOTAL_A']) > 0)
                                                <p>отвеченных: {{ $ctField['TOTAL_A'] }}</p>
                                                <?$itogCt += $ctField['TOTAL_A'] ?>
                                            @endif
                                            @if(($ctField['TOTAL_N']) > 0)
                                                <p>неотвеченных: {{ $ctField['TOTAL_N'] }}</p>
                                                <?$itogCt += $ctField['TOTAL_N'] ?>
                                            @endif
                                            @if(($ctField['TOTAL_B']) > 0)
                                                <p>занято во время звонка: {{ $ctField['TOTAL_B'] }}</p>
                                                <?$itogCt += $ctField['TOTAL_B'] ?>
                                            @endif
                                            @if(($ctField['TOTAL_F']) > 0)
                                                <p>ошибка оборудования: {{ $ctField['TOTAL_F'] }}</p>
                                                <?$itogCt += $ctField['TOTAL_F'] ?>
                                            @endif
                                        </td>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                            <p class="collection-item">
                                Общее количество за выбранный период: {{ $itogCt }}
                            </p>
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>
@endsection
