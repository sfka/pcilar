<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Отчет</title>

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/materialize.js') }}" defer></script>

    <!-- Styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="mobile_wrapper">
        <div class="container">
            <a class="brand-logo" href="{{ url('/') }}"></a>
            <a href="#" data-target="slide-out" class="sidenav-trigger btn">
                <i class="material-icons">меню</i>
            </a>
        </div>
    </div>
    <aside class="sidenav sidenav-fixed" id="slide-out">
        <div class="row">
            <div class="col s12">
                {{--                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"--}}
                {{--                        aria-controls="navbarSupportedContent" aria-expanded="false"--}}
                {{--                        aria-label="{{ __('Toggle navigation') }}">--}}
                {{--                    <span class="navbar-toggler-icon"></span>--}}
                {{--                </button>--}}

                <a class="brand-logo" href="{{ url('/') }}"></a>
                <div id="navbarSupportedContent" class="nav-items">
                    @can('manage-users')
                        <ul class="collection">

                            <li>
                                <a href="/">Отчет</a>
                            </li>
                            <li>
                                <a href="/leads">Лиды</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.properties.index') }}">Свойства</a>
                            </li>
                        </ul>
                    @endcan
                    @can('user-show-stats')
                        <ul class="collection">
                            <li>
                                {{--                            <a class="nav-link" href="{{ route('dashboard') }}">Отчет</a>--}}
                                <a href="/">Отчет</a>
                            </li>
                            <li>
                                <a href="/compare">Сравнения</a>
                            </li>
                            <li>
                                <a href="/leads">Лиды</a>
                            </li>
                        </ul>
                        <ul class="collection with-header">
                            <li class="collection-header">
                                <b>Все показатели</b>
                            </li>
                            <li>
                                <ul>
                                    <li>
                                        <a href="{{ route('google') }}">Google</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('yandex') }}">Yandex</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('calltrack') }}">CallTracking</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('facebook') }}">Facebook</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    @endcan
                </div>
                <ul class="user-actions">
                    <!-- Authentication Links -->
                    @guest
                        <li>
                            <a href="{{ route('login') }}">{{ __('Войти') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li>
                                <a href="{{ route('register') }}">{{ __('Регистрация') }}</a>
                            </li>
                        @endif
                    @else
                        <li>
                            <a class="dropdown-trigger btn" href="#" data-target="user_actions" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <ul id="user_actions" class="dropdown-content">
                                @can('manage-users')
                                    <li>
                                        <a href="{{ route('admin.users.index') }}">
                                            Управление пользователями
                                        </a>
                                    </li>
                                @endcan
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        {{ __('Выйти') }}
                                    </a>
                                </li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </aside>

    <main>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    @include('partials.alerts')
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
</div>
<div class="loader">
    <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="gap-patch">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>
</div>
<div id="tooltip-wrap"></div>
<script src="{{ asset('js/main.js') }}" defer></script>
</body>
</html>
