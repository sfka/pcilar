<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $managerRole = Role::where('name', 'manager')->first();
        $userRole = Role::where('name', 'user')->first();

        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'admin@created.com',
            'password' => Hash::make('password'),
            'googleLogin' => '',
            'yandexLogin' => '',
            'yandexCounterId' => '',
            'facebookLogin' => '',
            'callTrackLogin' => '',
            'callTrackPassword' => '',
            'callTrackProject' => '',
        ]);
        $manager = User::create([
            'name' => 'Manager User',
            'email' => 'manager@created.com',
            'password' => Hash::make('password'),
            'googleLogin' => '',
            'yandexLogin' => '',
            'yandexCounterId' => '',
            'facebookLogin' => '',
            'callTrackLogin' => '',
            'callTrackPassword' => '',
            'callTrackProject' => '',
        ]);
        $user = User::create([
            'name' => 'Not Admin User',
            'email' => 'user@created.com',
            'password' => Hash::make('password'),
            'googleLogin' => '331-787-2517',
            'yandexLogin' => 'e-16444694',
            'yandexCounterId' => '54123979',
            'facebookLogin' => '374957336448694',
            'callTrackLogin' => 'mirokon-ilar@yandex.ru',
            'callTrackPassword' => 'ILARlp2',
            'callTrackProject' => '8552',
        ]);

        $admin->roles()->attach($adminRole);
        $manager->roles()->attach($managerRole);
        $user->roles()->attach($userRole);
    }
}
